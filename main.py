from pyodide_mkdocs_theme.pyodide_macros import PyodideMacrosPlugin
from urllib.parse import unquote  # Pour tri par sélection


# Macro de Nicolas Revéret tris 

def define_env(env:PyodideMacrosPlugin):

    def convert_url_to_utf8(nom: str) -> str:
        return unquote(nom, encoding="utf-8")

    @env.macro
    def insert_html(html_file: str = "") -> str:
        """
        Purpose : Insert content of a html file in a Mkdocs document. {fichier_html}.py is loaded in the page
        """
        print("docs_dirs", env.conf["docs_dir"])
        path_file = "/".join(
            filter(
                lambda folder: folder != "",
                convert_url_to_utf8(env.variables.page.abs_url).split("/")[2:-2],
            )
        )
        docs_path = """docs/"""

        f = open(f"""{docs_path}/{path_file}/{html_file}.html""", encoding="utf-8")
        content = "".join(f.readlines())
        f.close()
        content = content + "\n"
        return content