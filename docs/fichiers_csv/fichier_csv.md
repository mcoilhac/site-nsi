---
author: Mireille Coilhac
title: Fichiers CSV
---

## Cours et exercices

!!! info ""
	
	Commencer par étudier **en entier** le cours suivant :

	[Fichiers CSV par Nicolas Revéret](https://nreveret.forge.apps.education.fr/donnees_en_table/){ .md-button target="_blank" rel="noopener" }

!!! info "Précisions sur la méthode `readline`"

	Nous avons vu [ici](https://nreveret.forge.apps.education.fr/donnees_en_table/2_fichiers_csv/3_import_listes/){:target="_blank" } : 

	```python hl_lines="3 4" linenums="1" title=""
	temperatures = []
	with open(file="temperatures_2020.csv", mode="r", encoding="utf-8") as fichier:
    	fichier.readline()  
    	for ligne in fichier:  
        	ligne_propre = ligne.strip()  
        	valeurs = ligne_propre.split(",")  
        	temperatures.append(valeurs)
	```

	La méthode `readline` lit la ligne suivante dans le fichier (les caractères à partir de la position actuelle du pointeur de fichier jusqu'au premier caractère de fin de ligne rencontré, souvent `\n` pour les fichiers texte), **à partir de la position actuelle du pointeur de fichier**. Elle renvoie la chaîne de caractères correspondante, et incrémente le pointeur de fichier.  
	La ligne 3 de ce script permet donc de lire la première ligne du fichier, qui contient les descripteurs (ceux-ci ne sont pas récupérés ici), **et incrémente le pointeur de fichier.**   
	👉 Ainsi la ligne 4 de ce script ne parcourt le fichier qu'à partir de la deuxième ligne de ce fichier, comme indiqué [ici](https://nreveret.forge.apps.education.fr/donnees_en_table/2_fichiers_csv/3_import_listes/){:target="_blank" }: 👓 Parcours et lecture **de toutes les lignes restantes** 👓

{{ multi_qcm('qcm_csv.json', shuffle_questions=True) }}
