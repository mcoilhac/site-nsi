---
author: Mireille Coilhac
title: Ressources
---

## Choix des spécialités

* Dans Parcoursup, depuis cette année 2025, on peut voir la probabilité d’être admis suivant le couple de spécialité choisi et sa moyenne générale. Il faut choisir une formation, puis sélectionner l’onglet « Visualiser les chiffres d’accès à la formation » : [Parcoursup](https://dossier.parcoursup.fr/Candidat/carte){ .md-button target="_blank" rel="noopener" }

* Pour aider au choix en fin de première : [Parcoursup en fin de première](https://www.parcoursup.gouv.fr/construire-son-projet-d-orientation/comment-choisir-vos-specialites-en-seconde-et-premiere-1619){ .md-button target="_blank" rel="noopener" }

* Quelques chiffres : [Site suptracker](https://beta.suptracker.org/?tab=page_accueil&type_bac=tous){ .md-button target="_blank" rel="noopener" }

[Diaporama présentant la spécialité NSI de terminale](a_telecharger/Spe_NSI_pour_term_2025.pdf){ .md-button target="_blank" rel="noopener" }

[Parcours de formation au campus Saint-Aspais](a_telecharger/PARCOURS%20FORMATIONS_Campus_St_Aspais.pdf){ .md-button target="_blank" rel="noopener" }

[Chaine youtube du commandement de la cyberdéfense](https://www.youtube.com/@Comcyber){ .md-button target="_blank" rel="noopener" }





