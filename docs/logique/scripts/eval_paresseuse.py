def vrai():
    print("Appel de la fonction vrai")
    return True

def faux():
    print("Appel de la fonction faux")
    return False

print("vrai() or faux() : ")
vrai() or faux()
print("**************************")
print("vrai() and faux()")
vrai() and faux()
print("**************************")
print("faux() and vrai()")
faux() and vrai()
