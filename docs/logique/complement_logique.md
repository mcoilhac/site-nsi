---
author: Mireille Coilhac
title: Compléments - Logique 
---

!!! danger "Attention"

    N'étudier ce qui suit qu'après avoir étudié les fonctions en Python


## I. Les redondances : mauvaise pratique

???+ question "Strictement négatif"

    Tester ci dessous : 

    {{IDE('scripts/strict_negatif')}}

    Les deux fonctions `est_strictement_negatif_1` et `est_strictement_negatif_2` sont-elles équivalentes ?

    ??? success "Solution"

        Ces deux fonctions sont équivalentes, et il est de bonne pratique de ne pas utiliser `est_strictement_negatif_1` mais  `est_strictement_negatif_2`.

        En effet, `nbre < 0` est une expression booléenne évaluée à `True` ou `False`. La valeur renvoyée est celle de `nbre < 0`

???+ question "Multiples de 6"

    Compléter en une ligne (ligne 12) la fonction `est_multiple_de_6_v2` pour qu'elle renvoie `True` si ,`nbre` est un multiple de 6
     et `False` sinon.

    {{ IDE('scripts/multiples_6', MAX_SIZE=20) }}

    ??? success "Solution"

        ```python title=""
        def est_multiple_de_6_v2(nbre):
            est_multiple_de_3 = (nbre % 3 == 0)
            est_multiple_de_2 = (nbre % 2 == 0)
            return est_multiple_de_3 and est_multiple_de_2
        ```


!!! abstract "Résumé"

    !!! abstract "Premier cas"

        Il est préférable de remplacer : 

        ```python title=""
        def fonction():
            if expression_a_valeur_booleenne: # l'expression est une condition
                return True
            else:
                return False
        ```

        par 

        ```python title=""
        def fonction():
            return expression_a_valeur_booleenne
        ```

    !!! abstract "deuxième cas"

        Il est préférable de remplacer : 

        ```python title=""
        def fonction():
            if expression_a_valeur_booleenne: # l'expression est une condition
                return False
            else:
                return True
        ```

        par 

        ```python title=""
        def fonction():
            return not expression_a_valeur_booleenne
        ```



## II. Evaluation séquentielle : 

???+ question "Tester"

    Essayer de comprendre ce qu'il se passe :

    {{IDE('scripts/eval_paresseuse', MAX_SIZE=18, TERM_H=20)}}

    ??? success "Solution"

        * `True` or ... est évalué automatiquement à `True`. Pas la peine d'évaluer ce qui est à droite du `or`
        * `False` and ... est évalué automatiquement à `False`. Pas la peine d'évaluer ce qui est à droite du `or`
        * Pour les autres cas, il faut évaluer ce qui est à gauche et ce qui est à droite de l'opérateur booléen.

    Vous pouvez ajouter vos propres essais.

???+ question "Tester"

    Essayer de comprendre ce qu'il se passe :

    {{IDE('scripts/racine_carree')}}

    ??? success "Solution"

        Si à gauche de l'opérateur `and` se trouve `False`, le membre de droite ne sera pas évalué. La fonction `sqrt` ne sera pas appelée si x est négatif (ce qui provoquerait une erreur que l'on observe si on intervertit les deux membres).


## III. Conversions implicites utilisées par Python

!!! danger "Attention"

    Ne pas utiliser ce qui suit. Nous le mentionnons pour comprendre certains comportements qui pourraient étonner.

???+ question "Tester"

    Essayer de comprendre ce qu'il se passe :

    {{IDE('scripts/conversion')}}

    ??? success "Solution"

        `True` est converti en 1 et `False` en 0

## IV. Attention au langage courant 

???+ question "Un jeu de dé"

    Le jeu consiste à lancer un dé. Si on obtient 1 ou 6, on gagne, sinon on perd.

    Un élève a écrit le code suivant : le tester, puis le corriger

    {{IDE('scripts/jeu_faux')}}

    ??? success "Solution"

        Le comportement n'est évidemment pas celui qui est attendu. En effet pour mieux comprendre ce qu'il se passe, 
        on peut ajouter des parenthèses. Les opérateurs de comparaison étant prioritaires sur les opérateurs logiques, nous obtenons :   
        
        `if (nbre == 1) or (6)`.  
        
        * `(nbre == 1)` est bien évalué à `True` ou `False` suivant les cas. 
        * Par contre 6 est un entier différent de zéro, donc 
        il est évalué en `True`, (0 serait évalué à `False`).  

        La ligne 2 est donc équivalente à `if (nbre == 1) or True`, ce qui est toujours évalué à `True`.

        Une solution correcte est donc : 

        {{IDE('scripts/jeu_juste')}}

