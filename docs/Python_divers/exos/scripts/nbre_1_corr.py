def compte_uns(tableau):
    if tableau == []:
        return 0

    if tableau[0] == 1:
        return len(tableau)

    debut = 0
    fin = len(tableau) - 1
    while debut <= fin:
        milieu = (debut + fin) // 2
        if tableau[milieu] == 0:
            debut = milieu + 1
        elif tableau[milieu - 1] == 1:
            fin = milieu - 1
        else:
            return len(tableau) - milieu

    return 0
