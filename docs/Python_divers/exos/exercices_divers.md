---
author: Mireille Coilhac
title: Exercices divers
---

## Jeu "plus ou moins"

[CodEx - Jeu plus ou moins](https://codex.forge.apps.education.fr/exercices/jeu_plus_moins/){ .md-button target="_blank" rel="noopener" }


## Les costumes

[CodEx - Les costumes](https://codex.forge.apps.education.fr/exercices/distribution_costume/){ .md-button target="_blank" rel="noopener" }


## Couper un jeu de cartes

[CodEx - Couper un jeu de cartes](https://codex.forge.apps.education.fr/exercices/couper_cartes/){ .md-button target="_blank" rel="noopener" }



