---
author: Nicolas Revéret
title: Nombre de 1
tags:
  - 5-diviser/régner
---

## Nombre de 1

On considère un tableau vide ou ne contenant que des 0 et des 1. Ce tableau est trié dans l'ordre croissant et il est possible qu'il ne contienne que des 0 ou que des 1.
Combien compte-t-il de 1 ?

Écrire la fonction `compte_uns` qui prend en paramètre un tel tableau et renvoie le nombre de 1 qu'il contient.

???+ warning "Attention"

    Certains des tableaux utilisés dans les tests sont très grands. Une méthode avec un coût linéaire sera inefficace face à ceux-ci.

    On limite donc le nombre de lectures dans chaque tableau à 500. Passé cette valeur maximale, tout nouvel accès provoquera une erreur.

    On rappelle à ce titre que le tableau est trié...


!!! example "Exemples"

    ```pycon
    >>> compte_uns([0, 1, 1, 1])
    3
    >>> compte_uns([0, 0, 0, 1, 1])
    2
    >>> compte_uns([0] * 200)
    0
    >>> compte_uns([1] * 300)
    300
    >>> compte_uns([0] * 200 + [1] * 500)
    500
    ```

???+ question "Exercice 1"

    Compléter le script ci-dessous :

    {{ IDE('scripts/nbre_1', MAX=5) }}


## Crédits

Un exercice de Nicolas Revéret

