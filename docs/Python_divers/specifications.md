---
author: Mireille Coilhac
title: Spécifications
---

<!--
!!! warning "Installations avant de commencer"


    **1.** Télécharger les deux fichiers ci-dessous, et les enregistrer dans un même dossier.

    🌐 TD à télécharger : Fichier `specifs.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/specifs.ipynb)

    🌐 Module à télécharger : Fichier `module_Alice.py` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/module_Alice.py)

    **2.** suivre ce lien, puis ouvrir le fichier `specifs.ipynb` : [Basthon](https://notebook.basthon.fr/){ .md-button target="_blank" rel="noopener" }

    **3.** Vous devez charger ce module : `module_Alice.py` dans le notebook ouvert sur Basthon :  
    icone "ouvrir fichier" puis choisir "Installer le module"

-->

???+ questions "Alice et Bob travaillent ensemble"

    Bob a besoin des fonctions créées par Alice pour écrire son propre programme.  
    
    **1.** Bob n'a pas sous les yeux le script du module d'Alice. Pour savoir quelles sont les fonctions créées par Alice, 
    sans ouvrir son fichier,comment va procéder Bob ? 

    Compléter ci-dessous

    {{ IDE('scripts/module_bob', ID=1) }}

    ??? success "Solution"

        En console : 

        ```pycon
        >>> dir(module_alice)
        ```

    **2.** Ecrire ci-dessous le code qui permet de connaître le rôle des fonctions écrites par Alice.

    {{ IDE('scripts/module_bob', ID=2) }}

    ??? tip "Astuce"

        Regardez bien comment a été réalisé l'importation du module d'Alice.

    ??? success "Solution"

        En console : 

        ```pycon
        >>> help(module_alice.fonction_1)

        ```

        puis

        ```pycon
        >>> help(module_alice.fonction_2)

        ```

<!--
???+ questions "A faire vous-même 1 et à faire vous-même 2 du TD"

    Vous pourrez vous référer au cours sur les modules

    ??? success "Solution A faire vous-même 1"

        `dir(module_Alice)`

    ??? success "Solution A faire vous-même 2"

        ```python
        help(module_Alice.fonction_1)
        help(module_Alice.fonction_2)
        ```
-->

???+ question "Bilan de l'exercice"


    **1.** Quelle est la syntaxe, et à quoi sert la spécification d'une fonction ?

    ??? success "Solution"

    	* La spécification s'écrit au début de la fonction entre `"""` et `"""`
    	* On parle aussi souvent de **"docstring"**
    	* Elle sert à préciser les paramètres, ce que renvoie la fonction, et son rôle.

    **2.** Quelle est la syntaxe, et à quoi sert la fonction `help` ?

    ??? success "Solution"

    	`help(ma_fonction)` renvoie la docstring de la fonction `ma_fonction`

        Si la fonction fait partie d'un module `mon_module.py` importé avec `import mon_module`, 
        il faut écrire : `help(mon_module.ma_fonction)`



