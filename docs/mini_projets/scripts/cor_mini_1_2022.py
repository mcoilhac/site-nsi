from random import randint    # Suffisant

def lancer_De():
    """ Creation d une liste de 10 lancers de des """
    return [randint(1, 6) for i in range(10)]

def points(liste):
    """ retourne le nombre de points de la liste"""
     # On compte le nombre de 6 puis  le nombre de points
    nombre_de_6 = 0  # Initialisation du nombre de 6 a zero
    points = 0       # Initialisation du nombre de points a zero
    for lancer in liste: # Parcour de la liste pour compter les 6
        if lancer == 6:
            nombre_de_6 = nombre_de_6 + 1
    if nombre_de_6 >= 2: # Comptage des points
        points = points + 1
    elif nombre_de_6 == 0:
        points = points - 1
    return points


# Programme principal
#####################

points_Alice = 0 # Initialisation du nombre de points d Alice
points_Bob = 0   # Initialisation du nombre de points d Alice

# Simulation du jeu tant qu Alice ou Bob n ont pas 5 points

while points_Alice < 5 and points_Bob < 5:
    liste_Alice = lancer_De() # Alice joue
    points_Alice = points_Alice + points(liste_Alice)  # Comptage des points d Alice
    print("Alice:", liste_Alice)
    print("Le nombre de point d'Alice est:", points_Alice)
    if points_Alice < 5 :
        liste_Bob = lancer_De() # Bob joue
        points_Bob = points_Bob + points(liste_Bob)   # Comptage des points de Bob
        print("Bob:", liste_Bob)
        print("Le nombre de point de Bob est:", points_Bob)

# Determination du gagnant

if points_Alice == 5:
    print("Alice a gagné")
elif points_Bob == 5:
    print("Bob a gagné")

# Jeu de tests
###############

assert min(lancer_De())>=1,"la liste ne doit contenir que des entiers entre 1 et 6"
assert max(lancer_De())<=6,"la liste ne doit contenir que des entiers entre 1 et 6"
assert len(lancer_De())==10, "la liste ne contient pas 10 termes"
assert points([1,3,6,1,3,2,5,1,4,3]) == 0, "pas de points pour moins de deux 6"
assert points([1,3,2,1,3,2,5,1,4,3]) == -1, "pas de points pour moins de deux 6"
assert points([1,3,6,1,6,2,5,1,4,3]) == 1, "1 point pour deux 6 ou plus"
assert points([1,3,6,1,6,2,5,6,4,3]) == 1, "1 point pour deux 6 ou plus"


