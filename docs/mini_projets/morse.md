## Le morse

L'alphabet morse, ou code morse, est un code permettant de transmettre un texte à l'aide de séries d'impulsions courtes et longues. Inventé en 1835 par Samuel Morse pour la télégraphie, ce code assigne à chaque lettre, chiffre et signe de ponctuation une combinaison unique de signaux intermittents. Considéré comme le précurseur des communications numériques, le code morse a depuis le 1er février 1999 été délaissé au profit d'un système satellitaire pour les communications maritimes.

C'est en 1838 que naît l'alphabet " morse " que nous connaissons. Deux types d'impulsions sont utilisés. Les impulsions courtes (notées " . ", point) qui correspondent à une impulsion électrique de 1/4 de seconde et les longues (notées " - ", trait) à une impulsion de 3/4 de seconde.

<img src="https://www.techno-science.net/illustration/Definition/inconnu/i/Intcode.png">

[Source](https://www.techno-science.net/definition/3815.html)


!!! example "Exemple"

    ```texte
    -°-° --- -°° °          -- --- °-° °°° °
    
     C    O   D  E           M  O   R   S   E 
    ```

!!! info "Utilisation d'un dictionnaire"

    On représente le code morse à l'aide d'un dictionnaire, on ne s'intéresse qu'aux lettres en majuscules non accentuées.  
    Pour l'espace on utilise le slash (par exemple).
    Vous pourrez recopier dans votre code ce dictionnaire.

    ```python
    morse = {' ': '/', 'E': '°', 'I': '°°', 'S': '°°°', 'H': '°°°°', 'V': '°°°-', 'U': '°°-', 'F': '°°-°',
    'A': '°-', 'R': '°-°', 'L': '°-°°', 'W': '°--', 'P': '°--°', 'J': '°---', 'T': '-', 'N': '-°', 'D': '-°°',
    'B': '-°°°', 'X': '-°°-', 'K': '-°-', 'C': '-°-°', 'Y': '-°--', 'M': '--', 'G': '--°', 'Z': '--°°', 
    'Q': '--°-', 'O': '---'}
    ```

### Travail à faire : 

#### 1. Ecrire un script qui permet de chiffrer un message en morse

!!! example "Exemple d'exécution"

    Vous pouvez avoir des noms de fonctions différents. Ceci n'est qu'un exemple.

    ```pycon
    >>> message = 'VIVE LA NSI'
    >>> code_mots(message, morse)
    '°°°-*°°*°°°-*°*/°-°°*°-*/-°*°°°*°°*'
    ```


#### 2. Ecrire un script qui permet de déchiffrer un message envoyé en morse.

!!! example "Exemple d'exécution"

    Vous pouvez avoir des noms de fonctions différents. Ceci n'est qu'un exemple.

    ```pycon
    >>> message = 
    '-°°°*°-°*°-*°°°-*---*/*°---*°*°°-*-°*°*/*°--°*°-*-°°*°-*°--*°-*-°*/*°-°°*°-*/*-°*°°°*°°*/*°*°°°*-*/*°-*°°°-*°*-°-°*/*-*---*°°*'
    >>> decode_mots(message, morse)
    'BRAVO JEUNE PADAWAN LA NSI EST AVEC TOI'
    ```


??? tip "Astuce : `split`"

    Tester : 

    {{ IDE('scripts/syntaxe_split') }} 


!!! info "Comment rendre son travail"

    Vous rendrez votre fichier python sur Classroom. Le nom du fichier doit être formé avec le votre.  

    Par exemple : dupond.py


!!! warning "Contraintes"

    * Le script sera bien structuré, avec plusieurs fonctions
    * Vous utiliserez le dictionnaire donné

