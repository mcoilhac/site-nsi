---
author: Mireille Coilhac
title: Introduction aux tuples
---

😊 Nous allons faire une petite digressions dans ce chapitre consacré aux tableaux, pour parler d'une autre structure de donnée linéaire : les **tuples**.

Nous les utilserons en effet dans quelques exercices dans ce cours, et vous aurez vite compris comment cela fonctionne.

## I. Présentation

!!! example "Exemples"

    ```python
    tuple_1 = (2, 1, 7, 0)  # Un tuple de nombres entiers
    tuple_2 = ("Alice", "Bob", "Charles")  # Un tuple de str
    ```


!!! info "Présentation des tuples"

    Un tuple (en français p-uplet) est un ensemble de valeurs, regroupées en un seul objet. Cela ressemble donc beaucoup aux listes, mais la différence est qu'on ne peux pas modifier les éléments d'un tuple. 

!!! info "Syntaxe"

	Un tuple contient des valeurs, séparées par des virgules, mais encadrées par des parenthèses, **( )** ce qui le différencie d'une liste.

    !!! example "Exemple"

        ```python
        tuple_1 = (2, 1, 7, 0) # tuple_1 est un tuple de nombres entiers
        tuple_2 = ("albert", "paul", "jacques") # tuple_2 est un tuple de `str`
        tuple_3 = ("1G4","NSI", 22, 13.7, True) # tuple_3 contient des éléments de différents types. 
        ma_liste = [2, 1, 7, 0] # ma_liste est une **liste** de nombres entiers
        ```

!!! info "Les types des tuples"

	Les tuples sont de type **`tuple`**


!!! info "Indices"

	Les éléments d'un tuples sont indicés de la même façon que ceux d'une liste (indice **0** pour le premier élément).

???+ question "Les tuples sont immuables"

    {{IDE('scripts/tuple_imm')}}


??? success "A savoir"

    Il est impossible de modifier les éléments d'un tuple.


!!! info "Syntaxes communes avec celles utilisées pour les listes"

    Les tuples, sont comme les listes, des séquences. Les syntaxes pour la longueur de la séquences (`len`), ou pour les parcours par indices ou par valeurs sont identiques.

???+ question "Exercice 1"

    Compléter le script ci-dessous 

    {{IDE('scripts/amis_indices')}}

??? success "Solution"

    ```python
    for i in range(len(amis)):
        print(amis[i])
    ```


???+ question "Exercice 2"

    Compléter le script ci-dessous 

    {{IDE('scripts/amis_valeurs')}}
    

??? success "Solution"

    ```python
    for ami in amis:
        print(ami)
    ```

## II. QCM 

???+ question "QCM"

    Cocher toutes les affirmations correctes pour chaque question.

    On considère : 
    
    ```python
    prix = [3, 5, 2, 1, 6]
    produits = ('cahier', 'livre', 'stylo', 'gomme', 'feutres')
    ``` 

    Que se passe-t-il si on exécute :

    {{multi_qcm(
      ["`print(prix[0])`", ["3", "un message d'erreur", "0","6"], [1]],
      ["`print(produits(1))`", ["`#!py cahier`", "`#!py livre`", "un message d'erreur", "1"], [3]],
      ["`print(produits[1])`", ["`#!py cahier`", "`#!py livre`", "un message d'erreur", "1"], [2]],
      ["`prix[4] = 5`", ["`#!py prix` devient `#!py [3, 5, 2, 1, 5]`", "`#!py prix` devient `#!py [3, 5, 2, 5, 6]`", "un message d'erreur"], [1]],
      ["`produits[4] = 'stylos'`", ["`#!py produits` devient `#!py ('cahier', 'livre', 'stylo', 'gomme', 'stylos')`", "`#!py produits` devient `#!py ('cahier', 'livre', 'stylo', 'stylos', 'feutres')`", "un message d'erreur"], [3]],
        multi = False,
        qcm_title = "Un QCM sur les tuples",
        shuffle = True,
 
    )}}



