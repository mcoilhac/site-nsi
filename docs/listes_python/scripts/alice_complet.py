# --- PYODIDE:env --- #

def creation_liste_tuples(n):
    """
    n est un entier donnant la taille de la liste à saisir
    La fonction renvoie la liste des tuples (note, coefficient) .
    """
    liste_tuples = []
    for i in range(n):
        note = float(input("Saisir votre note : "))
        coef = float(input("Saisir le coefficient correspondant : "))
        assert note >= 0 and note <=20, "la note est entre 0 et 20."
        assert coef >= 0, "le coefficient est positif"
        liste_tuples.append((note, coef))
    return liste_tuples

def moyenne(lst_notes):
    """
    lst_notes est une liste de tuples (note, coefficient)
    La fonction renvoie la moyenne des notes en tenant compte des coefficients
    """
    somme_notes = 0
    somme_coefs = 0
    for (note, coef) in lst_notes:
        somme_notes = somme_notes + note*coef 
        somme_coefs = somme_coefs + coef 
    resultat = round(somme_notes/somme_coefs, 2)
    return resultat

# --- PYODIDE:code --- #

...