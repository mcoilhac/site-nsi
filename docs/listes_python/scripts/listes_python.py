animaux = ["girafe", "tigre", "singe", "souris", "éléphant"]
animaux.append("serpent")
print(animaux)
animaux.sort()
print(animaux)
print(animaux.index("souris"))
animaux.pop()
print(animaux)
animaux.pop(2)
print(animaux)
print(animaux[1:3])
print("babouin" in animaux)
print("babouin" not in animaux)
print("singe" in animaux)

# Vos propres essais ici :




