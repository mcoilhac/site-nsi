# --- PYODIDE:env --- #

def simulation(n):
    """
    n est le nombre de parties que l'on va simuler
    """
    parties = [randint(1, 10) < 7 for i in range(n)]
    nbre_T = 0
    nbre_F = 0
    for elt in parties:
        if elt == True:
            nbre_T = nbre_T + 1
    for elt in parties:
        if elt == False:
            nbre_F = nbre_F + 1
    return (nbre_T, nbre_F)

# --- PYODIDE:code --- #

from random import randint

(nbre_T_10, nbre_F_10) = simulation(10)
(nbre_T_1000000, nbre_F_1000000) = simulation(1000000)
print("Pour 10 parties -> nombre de True : ", nbre_T_10, " - nombre de False : ", nbre_F_10)
print("Pour 1000000 parties -> nombre de True : ", nbre_T_1000000, " - nombre de False : ", nbre_F_1000000)

