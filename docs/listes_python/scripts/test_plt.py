# --- PYODIDE:env --- #

import matplotlib.pyplot as plt
fig1 = PyodidePlot('cible_1')  
fig1.target()

# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

# On dessine 3 points :(ro signifie open circle, red)
abscisses = [1, 3, 7]
ordonnees = [2, 1, 4]
plt.plot(abscisses, ordonnees, "ro", label = 'points rouge')

# On dessine ces mêmes 3  points joints par une ligne continue(b signifie blue):

plt.plot(abscisses, ordonnees, "b", label = 'ligne bleue')

# On dessine 3 autres points (dg signifie diamond, green):
abscisses = [1.3, 4, 5]
ordonnees = [2, 1, 4]
plt.plot(abscisses, ordonnees, "dg", label = 'losanges verts')

# On affiche les légendes (qui ont été spécifiées avec label =...)
plt.legend()
plt.show()

