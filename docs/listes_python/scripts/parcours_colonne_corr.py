def get_colonne(matrice, n_colonne) -> list :
    """
    Entrée : matrice : une liste de listes, un entier n_colonne le numéro de la colonne
    Sortie : colonne : une liste dont les éléments sont les éléments de la colonne numéro n_colonne
    """
    colonne = [0] * len(matrice)
    for i in range(len(matrice)) :
        colonne[i] = matrice[i][n_colonne]
    return colonne
    