# --- PYODIDE:code --- #

eleves = ["Amandine" , "Gregory" , "Eléonore" , "Pascal" , "Antoine" , "Tonio", "Bob"]
        
eleves_A_B = ...
print(eleves_A_B )


# --- PYODIDE:corr --- #

eleves_A_B  = [prenom for prenom in eleves if (prenom[0] == "A" or prenom[0] == "B")]


# --- PYODIDE:secrets --- #

assert eleves_A_B  == ['Amandine', 'Antoine', 'Bob'], "Mauvaise réponse"
