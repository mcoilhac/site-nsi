# --- PYODIDE:env --- #

def creation_liste(n):
    """
    n est un entier donnant la taille de la liste à saisir
    La fonction renvoie la liste saisie
    """
    liste_notes = []
    for i in range(n):
        note = float(input("saisir votre note : "))
        liste_notes.append(note)
    return liste_notes


# --- PYODIDE:code --- #

def moyenne(lst):
    """
    Cette fonction renvoie la moyenne de la liste lst
    """
    ...

n = int(input("nombre de notes à saisir : "))
notes = creation_liste(n)
print(notes)
moyenne_notes = ...
print(moyenne_notes)
