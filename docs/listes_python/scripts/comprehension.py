from random import randint
liste_1 = [9*i for i in range (1, 11)]
liste_2 = [2*nbre for nbre in liste_1]
liste_3 = [i for i in range(200) if i%11 == 0]
liste_4 = [randint(1, 6) == 6 for i in range(10)]
print(liste_1)
print(liste_2)
print(liste_3)
print(liste_4)

# Vos propres essais ici :
