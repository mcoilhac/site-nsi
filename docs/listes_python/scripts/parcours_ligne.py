# --- PYODIDE:code --- #

def get_ligne(lst, i) -> list :
    """
    Entrées : 
      - lst : une liste de listes
      - i :un entier
    Sortie : une liste dont les éléments sont les éléments de la ligne i
    """
    ...


# --- PYODIDE:corr --- #

def get_ligne(lst, i) -> list :
    """
    Entrées : 
      - lst : une liste de listes
      - i :un entier
    Sortie : une liste dont les éléments sont les éléments de la ligne i
    """
    return lst[i]

# --- PYODIDE:tests --- #

m = [[1, 3, 4],
     [5, 6, 8],
     [2, 1, 3],
     [7, 8, 15]]
assert get_ligne(m, 0) == [1, 3, 4]
assert get_ligne(m, 3) == [7, 8, 15]


# --- PYODIDE:secrets --- #

m2 = [[0, 1, 2, 3, 4, 5, 6],
    [7, 8, 9, 10, 11, 12, 13],
    [14, 15, 16, 17, 18, 19, 20]]

for i in range(3):
    assert get_ligne(m2, i) == m2[i]



