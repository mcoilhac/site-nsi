# --- PYODIDE:code --- #

points = [(1, 2), (-1, 1), (4, 3), (3, 7), (-2, -3), (0, 1)]
        
points_sur_droite = ...
print(points_sur_droite)


# --- PYODIDE:corr --- #

points_sur_droite = [elem for elem in points if 2 * elem[0] + 1 == elem[1]]


# --- PYODIDE:secrets --- #

assert points_sur_droite == [(3, 7), (-2, -3), (0, 1)], "Mauvaise réponse"
