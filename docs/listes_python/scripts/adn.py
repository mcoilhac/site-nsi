# --- PYODIDE:code --- #

seq_adn = ["AATGCTG", "TGAACTG", "ATCGAAT", "TCAACTG", "CGAATAT"]
        
seq_adn_AAT = ...
print(seq_adn_AAT )


# --- PYODIDE:corr --- #

seq_adn_AAT  = [element for element in seq_adn if "AAT" in element]


# --- PYODIDE:secrets --- #

assert seq_adn_AAT  == ['AATGCTG', 'ATCGAAT', 'CGAATAT'], "Mauvaise réponse"