liste_1 = [5, 1, 3, 2]
print(sorted(liste_1))
print(liste_1)  # liste_1 n'a pas été modifiée

"""
sorted est une fonction. On peut affecter ce qui est 
renvoyé par cette fonction à une variable
"""

liste_2 = [8, 4, 5]
liste_2_triee = sorted(liste_2)
print(liste_2_triee)  
print(liste_2)  # liste_2 n'a pas été modifiée