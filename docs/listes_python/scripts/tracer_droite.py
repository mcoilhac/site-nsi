# --- PYODIDE:env --- #

import matplotlib.pyplot as plt
fig2 = PyodidePlot('cible_2')  
fig2.target()

# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

# Pour tracer la droite, on veut une liste d'abscisses contenant [-2, -1,... 3]
# Créez cette liste en compréhension
abscisses = ...

# Pour les ordonnées, créez en compréhension une liste en utilisant abscisses 
# mais qui contienne les nombres 2 * x + 1
ordonnees = ...

# Et maintenant faire le dessin

plt.plot(abscisses, ordonnees, "b", label = "Droite d'équation y = 2 x + 1")
plt.grid()  # Optionnel : pour voir le quadrillage
plt.axhline()  # Optionnel : pour voir l'axe des abscisses
plt.axvline()  # Optionnel : pour voir l'axe des ordonnées
plt.legend()
plt.show()
