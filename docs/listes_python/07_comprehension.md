---
author: Pierre Marquestaut, Mireille Coilhac et Nicolas revéret
title: Construction par compréhension
---


## I. Premiers tableaux en compréhension

Un tableau peut s'initialiser avec des éléments par défaut de plusieurs manières.

En voici deux :

```python
>>> tableau = [0, 0, 0, 0]
>>> tableau = [0] * 4  
```

Le langage Python permet une autre manière pour créer des tableaux : il s'agit de la **construction par compréhension**.

Cela permet de créer des tableaux que nous n'aurions pas su créer avec la méthode précédente. Par exemple, comment créer le tableaux contenant les 1000 premiers carrés : 1, 4, 9, 16, 25 etc ? Nous allons étudier comment procéder dans ce cours.

Le code suivant permet également de créer un tableau de 4 éléments initialisés à 0. C'est une création de tableau **par compréhension**. on dit aussi que le tableau est **écrit en compréhension**

```python
>>> tableau = [0 for i in range(4)]
>>> tableau
[0, 0, 0, 0]
```
De la même façon, la boucle suivante :
```python
tableau = [0]*4
for i in range(4):
    tableau[i] = i
```
peut s'écrire :
```python
tableau = [i for i in range(4)]
```


???+ question

    ```python
    tableau = [i for i in range(5, 15)]
    ```

    === "Avec quelle ligne pourrait-on remplacer la ligne précédente ? (Cocher la réponse correcte)"
        
        - [ ] `#!py tableau = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]`
        - [ ] `#!py tableau = [i, i, i, i, i, i, i, i, i, i]`
        - [ ] `#!py tableau = [0, 0, 0, 0, 0, 0, 0, 0, 0]`
        - [ ] `#!py tableau = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14]`

    === "Solution"
        
        
        - :x: La valeur 15 est exclue.
        - :x: i prend les valeurs de l'intervalle.
        - :x: La valeur n'est pas contante.
        - :white_check_mark: i prend tour à tour les valeurs de 5 jusqu'à 14.

???+ question

    Ecrire en compréhension la liste `[5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5]` qui contient 20 fois l'entier 5.

    [Puzzle](https://www.codepuzzle.io/p/T3WK){ .md-button }


???+ question

    Compléter le script ci-dessous :

    {{IDE('scripts/construction')}}

    ??? success "Solution"

        ```python
        # un tableau cents en compréhension qui contient 10 entiers 100.
        cents = [100 for k in range(10)]

        # un tableau entiers en compréhension qui contient les 10 entiers entre 1 et 10 compris.
        entiers = [k for k in range(1, 11)]

        ```

On peut aussi utiliser des chaînes de caractères : 

???+ Testez

    Exécuter le script ci-dessous :

    {{IDE('scripts/avec_str')}}

???+ question

    Ecrire en compréhension : `['NSI-1', 'NSI-2', 'NSI-3', 'NSI-4', 'NSI-5', 'NSI-6', 'NSI-7', 'NSI-8', 'NSI-9', 'NSI-10']`

    {{IDE('scripts/a_vous')}}

    ??? success "Solution"

        ```python
        mon_tab = ["NSI-"+ str(i) for i in range(1, 11)]
        print(mon_tab)
        ```

### Quelques exemples

???+ question

    Compléter ci dessous le script : Ecrire en compréhension `lst3` qui donne la liste de la somme des éléments de [2, 3, 1, 5] et de [4, 1, 7, 0].
    On doit obtenir : [6, 4, 8, 5]

    {{IDE('scripts/somme_listes')}}

    ??? success "Solution"

        ```python
        lst1 = [2, 3, 1, 5]
        lst2 = [4, 1, 7, 0]
        lst3 = [lst1[p] + lst2[p] for p in range(len(lst1))]
        print(lst3)
        ```

???+ question

    Répondre sur **papier**.

    Donner les listes lst1, lst2, lst4, lst6 et lst7

    ```python
    lst1 = [3 for i in range(4)]
    lst2 = [4 - i for i in range(3)]
    lst3 = [1, 2, 3]
    lst4 = [lst3[i]**2 for i in range(len(lst3))]
    lst5 = ["a", "b", "c"]
    lst6 = [lst5[i] * 2 for i in range(len(lst5))]
    lst7 = [elem * 2 for elem in lst5]
    ```

    ??? success "Solution"

        ```python
        lst1 = [3, 3, 3, 3]
        lst2 = [4, 3, 2]
        lst4 = [1, 4, 9]
        lst6 = ['aa', 'bb', 'cc']
        lst7 = ['aa', 'bb', 'cc']
        ```


## II. Utilisation plus élaborée des tableaux en compréhension


Grâce à la construction par compréhension, il est possible d'appliquer un traitement (opération, fonction...) à chaque élément d'un tableau.

???+ Testez

    Exécuter le script ci-dessous :

    {{IDE('scripts/carres')}}


???+ question

    ```python
    double = [i*2 for i in range(10)]
    ```

    === "Avec quelle ligne pourrait-on remplacer la ligne précédente ? (Cocher la réponse correcte)"
        
        - [ ] `#!py double = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`
        - [ ] `#!py double = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]`
        - [ ] `#!py double = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]`
        - [ ] `#!py double = [4, 8, 12, 16]`

    === "Solution"
        
        
        - :x: Un traitement est appliqué aux valeurs prises par i.
        - :x: Le mauvais traitement est appliqué.
        - :white_check_mark: le tableau est composé du double de chaque valeur de l'intervalle [0,9]
        - :x: Le tableau double doit contenir autant d'éléments que le tableau d'origine.

???+ question

    Compléter pour obtenir la liste `dizaines = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]`

    [Puzzle](https://www.codepuzzle.io/p/RBEF){ .md-button }

        
## III. Appliquer un filtre

La construction par compréhension permet d'appliquer un filtre à une structure de données de départ, afin de ne garder que certains éléments. On utilise pour cela une condition précédée du mot-clé `if`.

On peut ainsi créer un tableau qui ne conserve que les nombres pairs d'un tableau initial.

```python
>>> tableau = [0, 1, 6, 5, 4, 11, 12, 23, 26]
>>> pairs = [p for p in tableau if p%2 == 0]
>>> pairs
[0, 6, 4, 12, 26]
```

???+ question 

    ```python
    tableau = [i for i in range(5, 15)]
    nouveau_tableau = [j for j in tableau if j < 10]
    ```

    === "Avec quelle ligne pourrait-on remplacer la deuxième ligne ? (Cocher la réponse correcte)"
        
        - [ ] `#!py nouveau_tableau = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14]`
        - [ ] `#!py nouveau_tableau = [5, 6, 7, 8, 9]`
        - [ ] `#!py nouveau_tableau = [9, 8, 7, 6, 5]`
        - [ ] `#!py nouveau_tableau = [10, 11, 12, 13, 14]`

    === "Solution"   
        
        - :x: La condition entraine la sélection de certaines valeurs.
        - :white_check_mark: on sélectionne tous les éléments du tableau inférieurs à 10.
        - :x: Les éléments conservent l'ordre dans lequel il se trouvent dans le tableau initial.
        - :x: La condition indique les éléments qui sont conservés.

???+ question

    Créer en compréhension la liste des carrés des nombres de la liste `nombres`qui sont négatifs.

    [Puzzle](https://www.codepuzzle.io/p/W7MP){ .md-button }

???+ question

    Compléter le script ci-dessous :

    {{IDE('scripts/ssensemble')}}

    ??? success "Solution"

        ```python
        # un tableau positifs en compréhension qui contient 
        # les nombres réels positifs du tableau nombres
        nombres = [1, 0, -2, 9, -5, 4, -7, 5, -8]
        positifs = [k for k in nombres if k > 0]

        # un tableau voyelle_phrase en compréhension qui ne contient que les voyelles 
        # contenues dans la chaine de caractère phrase
        phrase = "je ne suis pas sans voix !"
        VOYELLES = "aeiouy"
        voyelle_phrase = [caractere for caractere in phrase if caractere in VOYELLES]
        ```

## IV. Les tableaux de tableaux


Pour construire un tableau de tableaux de même longueurs, on peut utiliser des compréhensions imbriquées.

Dans les exemples qui suivent nous appelerons `matrice` notre tableau de tableaux.

```python
>>> matrice = [[k for k in range(4)] for j in range(3)]
>>> matrice
[[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]
```

Pour bien visualiser le tableau `matrice` que nous venons de créer, nous pouvons l'écrire de la façon suivante :

```python
    matrice = [ [0, 1, 2, 3],
                [0, 1, 2, 3],
                [0, 1, 2, 3] ]         
```
👉 Chaque élément de `matrice` correspond donc à une "ligne".

On peut considérer que `matrice` a quatre "colonnes". Par exemple la première colonne (de rang 0) est `[0, 0, 0]`, et la dernière est `[3, 3, 3]`.

💡 Il est possible d'extraire une ligne de `matrice`.

```python
>>> ligne_0 = matrice[0]
>>> ligne_0
[0, 1, 2, 3]
```

💡 Il est possible d'extraire une colonne de `matrice`.

```python
>>> colonne_2 = [ligne[2] for ligne in matrice]
>>> colonne_2
[2, 2, 2]
```
???+ question "QCM 1"

    ```python
    matrice = [[j for i in range(4)] for j in range(4)]
    ```

    === "Avec quelle ligne pourrait-on remplacer la ligne précédente ? (Cocher la réponse correcte)"
        
        - [ ] `#!py matrice = [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]]`
        - [ ] `#!py matrice = [[0, 0, 0, 0], [1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]]`
        - [ ] `#!py matrice = [[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]`
        - [ ] `#!py matrice = [0, 1, 2, 3]`

    === "Solution"   
        
        - :x: La valeur j est constante pour chaque ligne.
        - :white_check_mark: La valeur j prend la valeur 0 pour la première ligne, puis 1, etc.
        - :x: La valeur j est constante pour chaque ligne.
        - :x: Les constructions imbriquées engendrent un tableau de tableaux.
        

???+ question "Puzzle 1"

    Créer en compréhension la matrice `[[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]`

    [Puzzle](https://www.codepuzzle.io/p/KSER){ .md-button }


???+ question "À vous de jouer 1"

    Compléter le script ci-dessous :

    {{IDE('scripts/matrice')}}

    ??? success "Solution"

        ```python
        # un tableau matrice_carre_10 en compréhension, matrice 10x10
        # dont chaque ligne contient les entiers de 1 à 10.
        matrice_carre_10 = [[k for k in range(1, 11)] for j in range(10)]

        # un tableau ligne_5 en compréhension qui contient la colonne 5 (située à l'indice 4)
        # de la matrice matrice_carre_10
        colonne_5 = [ligne[4] for ligne in matrice_carre_10]

        # un tableau diagonale en compréhension qui contient les éléments situés 
        # à la 1ère colonne de la 1ère ligne,
        # à la 2ème colonne de la 2ème ligne ... de la matrice carrée matrice_carre_10
        diagonale = [matrice_carre_10[i][i] for i in range(len(matrice_carre_10))]
        ```

???+ question "À vous de jouer 2"

    Compléter la fonction `donne_colonne` qui prend en paramètres une matrice `matrice`, un entier `colonne` et renvoie la liste des éléments de la colonne `colonne`. 

    Nous avons déjà réalisé cet exercice dans le chapitre : listes de listes. Vous devez ici **absolument** utiliser une liste en compréhension.


    Par exemple :

    ```pycon
    >>> m = [[1, 3, 4],
        [5, 6, 8],
        [2, 1, 3],
        [7, 8, 15]]
    >>> donne_colonne(m, 0)
    [1, 5, 2, 7]
    >>> donne_colonne(m, 2)
    [4, 8, 3, 15]
    >>> 
    ```

    {{IDE('scripts/parcours_colonne_v2')}}

???+ question "À vous de jouer 3"

    Compléter le script ci-dessous :

    👉 Vous pouvez tester les `###` en haut à droite de la fenêtre.

    {{IDE('scripts/matrice_diag_2', MAX = 4)}}

    ??? success "Solution"

        ```python
        def get_diag_2(lst) -> list :
        """
        Entrée : lst : une liste de listes de mêmes longueurs
        Sortie : diag_2 : une liste dont les éléments sont les éléments de 
        la 2eme diagonale (de en bas à gauche à en haut  droite)
        >>> m = [ [1, 3, 4],
                [5 ,6 ,8],
                [2, 1, 3] ]
        >>> get_diag_2(m)
        [4, 6, 2]

        """

        indice_max = len(lst) - 1
        return [lst[i][indice_max-i] for i in range(len(lst))]
        ```
        

## V. Exercices

### 1. Exercices variés

??? question "Nombres pairs"

    Ecrire en compréhension le liste des cinq premiers entiers pairs.

    {{ terminal() }}

    ??? success "Solution"

        ```pycon title=""
        >>> mon_tab = [2 * p for p in range(5)]
        ```

        Vérifier avec : 

        ```pycon title=""
        >>> mon_tab 
        ```

??? question "À vous de jouer 1 : Table de multiplication"

    Ecrire La liste des entiers de la table de multiplication de 9 : `[9, 18, ..., 90]`

    {{ terminal() }}

    ??? success "Solution"

        ```pycon title=""
        >>> tab_9 = [9*i for i in range(1, 11)]
        ```

        Vérifier avec : 

        ```pycon title=""
        >>> tab_9 
        ```



??? question "À vous de jouer 2 : les puissances de 2"

    Ecrire en compréhension la liste suivante des tuples donnant les puissances de 2 :  
    `[(0, 1), (1, 2), (2, 4), (3, 8), (4, 16), (5, 32), (6, 64), (7, 128)]`

    {{ terminal() }}

    ??? success "Solution"

        ```pycon title=""
        >>> mon_tab = [(p, 2**p) for p in range(8)]
        ```

        Vérifier avec : 

        ```pycon title=""
        >>> mon_tab 
        ```

??? question "Une liste dans une liste en compréhension"

    Tester :

    {{IDE('scripts/moins_1')}}

??? question "Des filtres"

    Tester :

    {{ IDE('scripts/modulo_11') }}

??? question "À vous de jouer 3"

    Ecrire la liste des entiers inférieurs à 100 qui sont à la fois multiples de 3 et de 5.

    {{ terminal() }}

    ??? success "Solution"

        ```pycon title=""
        >>> reponse = [i for i in range(100) if i % 3 == 0 and i % 5 == 0]
        ```

        Vérifier avec : 

        ```pycon title=""
        >>> reponse 
        ```

??? question "À vous de jouer 4"

    On donne une liste de coordonnées de points : `points`.
    Compléter le script pour afficher la liste des coordonnées des points se trouvant sur 
    la droite d'équation $y=2x+1$

    {{ IDE('scripts/sur_droite') }}

??? question "À vous de jouer 5"

    On donne une liste d'élèves d'un groupe : `#!py lst_eleves`. Compléter le script pour 
    écrire une liste en compréhension qui contient la liste des élèves dont le nom commence par A ou B.

    {{ IDE('scripts/eleves') }}

??? question "À vous de jouer 6"

    On donne une liste de séquences ADN. Ecrire une liste extraite de la précédente, avec seulement les séquances contenant `"AAT"`

    {{ IDE('scripts/adn') }}

??? question "À vous de jouer 7 : simuler un jeu"

    💡 Encore plus fort : utilisation du booléen.

    **1.** Tester ci-dessous : 

    {{ IDE('scripts/avec_bool') }}

    À quoi sert `#!py ma_liste_2` ?

    ??? success "Solution"

        Cette liste permet de simuler 20 lancers de dés, et de noter pour chacun True ou False suivant que le dé est tombé 
        sur 6 ou non.

    **2.** On va simuler le jeu suivant :  
    On tire au hasard un numéro parmi 1, 2, 3, 4, 5, 6, 7, 8, 9 et 10.  
    Si le numéro obtenu est strictement inférieur à 7 (parmi 1, 2, 3, 4, 5, 6), on gagne un bonbon. Sinon, on perd.  
    On veut créer la liste `#!py parties` de 50 résultats obtenus, si l'on joue 50 fois.  
    On obtient le booléen `#!py True` lorsque l'on gagne, et le booléen `#!py False` lorsque l'on perd. La liste `#!py parties` est donc composée de ces 50 booléens.  
    A-t-on plus de chance de gagner ou de perdre ?  
    Compléter ci-dessous pour qu'il s'affiche le nombre de `#!py True` et le nombre de `#!py False` de la liste.  
    Exécuter votre code plusieurs fois pour observer les résultats obtenus.

    {{ IDE('scripts/simul_jeu') }}

    ??? success "Solution"

        ```python title=""
        from random import randint
        parties = [randint(1, 10) < 7 for i in range(50)]
        print(parties)
        nbre_T = 0
        nbre_F = 0
        for elt in parties:
            if elt == True:
                nbre_T = nbre_T + 1
            else:
                nbre_F = nbre_F + 1
        print("nombre de True : ", nbre_T)
        print("nombre de False : ", nbre_F)
        ```

    **3.** Dans la question **2.** nous avons simulé 50 parties. Nous voulons maintenant pouvoir simuler un nombre quelconque de parties.  
    Compléter la fonction `simulation` qui prend en paramètre un entier `#!py n` et renvoie un tuple dont le premier élément est le 
    nombre de `#!py True` et le second le nombre de `#!py False` obtenus pour `#!py n` parties. Il ne faut pas faire afficher les listes obtenues.  
    Compléter également le script pour qu'il s'affiche le nombre de `#!py True` et de `#!py False` respectivement pour 100 puis 1000 parties.

    {{ IDE('scripts/simul_jeu_n') }}

    ??? success "Solution"

        ```python title=""
        from random import randint
        def simulation(n):
            """
            n est le nombre de parties que l'on va simuler
            """
            parties = [randint(1, 10) < 7 for i in range(n)]
            nbre_T = 0
            nbre_F = 0
            for elt in parties:
                if elt == True:
                    nbre_T = nbre_T + 1
                else:
                    nbre_F = nbre_F + 1
            return (nbre_T, nbre_F)

        (nbre_T_100, nbre_F_100) = simulation(100)
        (nbre_T_1000, nbre_F_1000) = simulation(1000)
        print("Pour 100 parties -> nombre de True : ", nbre_T_100, " - nombre de False : ", nbre_F_100)
        print("Pour 1000 parties -> nombre de True : ", nbre_T_1000, " - nombre de False : ", nbre_F_1000)
        ```

    **4.** Plus on prend des échantillons de grande taille, plus les effectifs obtenus se rapprochent de ceux calculés avec des probabilités théoriques :   
    ici 60% de  `True` et 40% de `False` .  
    Vous pouvez tester avec 10 parties (😢trop petit) ou 1000000 de parties (🤣déjà grand!)

    ??? note pliée "Tester ici"

        La fonction `simulation` n'a pas été écrite, car elle est dans du code caché.

        {{ IDE('scripts/simul_jeu_n_autres') }}


??? question "À vous de jouer 8 : réaliser des graphiques avec matplotlib"

    La bibliothèque matplotlib fonctionne avec des listes d'abscisses et d'ordonnées (qui doivent être de même tailles)

    **1.** Tester ci-dessous pour comprendre les syntaxes: 

    {{ IDE('scripts/test_plt') }}

    {{ figure('cible_1') }}

    **2.** Compléter ci-dessous pour tracer la droite d'équation $y=2x+1$ pour $-2 \leq x \leq 3$  
    Toutes es listes doivent être écrites en compréhension.

    {{ IDE('scripts/tracer_droite') }}

    {{ figure('cible_2') }}

    ??? success "Solution"

        ```python title=""
        abscisses = [i for i in range(-2, 4)]
        ordonnees = [2*x + 1 for x in abscisses]
        ```


 
### 2. "π à Monte-Carlo"

??? question "Sujet - π à Monte-Carlo"

    La [méthode de Monte-Carlo](https://fr.wikipedia.org/wiki/M%C3%A9thode_de_Monte-Carlo) est un ensemble de méthodes algorithmiques visant à déterminer la valeur approchée d'une constante en utilisant des procédés aléatoires.

    On peut utiliser cette méthode afin de déterminer une valeur approchée de $\pi$. L'idée est la suivante :

    * on considère un carré de $2$ unités de côtés. Son aire vaut donc $4$ ;
    * on considère un disque de rayon $1$ centré au centre du carré. Son aire vaut donc $\pi \times 1^2=\pi$ ;
    * on génère un grand nombre de points aléatoires répartis de façon uniforme dans le carré.
    
    Il reste alors à compter le nombre de points à l'intérieur du disque. On peut montrer que leur fréquence tend vers $\frac{\pi}{4}$ quand le nombre de points aléatoires devient très grand.
    
    Une valeur approchée de $\pi$ est donc :
    
    $$\pi \approx 4 \times \frac{\text{nombre de points dans le disque}}{\text{nombre de points dans le carré}}$$

    On observe ci-dessous le carré de départ ainsi que de nombreux points. On a représenté de couleur différente ceux qui sont dans le cercle et ceux qui n'y sont pas.

    ![Méthode de Monte-Carlo](images/monte_carlo.svg){ .center .autolight width=50%}

    On se donne donc : 

    * une liste de `#!py nb_points` aléatoires, tous dans le carré décrit ci-dessus. Cette liste est nommée `points` et chaque point est représenté par ses coordonnées. Par exemple `#!py [(-0.5313, 0.0936), (0.9638, 0.3577), ...]`.
    
    * une fonction `#!py distance_origine` prenant en argument les coordonnées `x` et `y` d'un point et renvoyant sa distance à l'origine du repère (et donc au centre du cercle)

    On demande d'extraire la liste des points situés dans le cercle à l'aide d'une liste en compréhension.
    

??? note "La fonction `#!py random`"

    Le module `#!py random` de Python propose une fonction `#!py random` qui génère des nombres aléatoires uniformément répartis entre `#!py 0` et `#!py 1`.
        
    👉 On a donc `#!py 2 * random()` qui est compris entre `#!py 0` et `#!py 2`

    👉 On en déduit que  `#!py 2 * random() - 1` est compris entre `#!py -1` et `#!py 1`. 


??? question "À vous de jouer"   

    {{ IDE('scripts/points') }}


??? success "Solution"

    Pour ne pas surcharger le site, nous avons choisi ici `nb_points = 1000`. Chez vous, sur votre prpre éditeur Python, vous pouvez tester avec `nb_points = 100_000`, pour obtenir une meilleure précision.

    ```python
    from math import sqrt
    from random import random

    def distance_origine(x, y):
        return sqrt(x*x + y*y)

    nb_points = 1000
    points = [(2 * random() - 1, 2 * random() - 1) for _ in range(nb_points)]
    dans_cercle = [p for p in points if distance_origine(p[0], p[1]) <= 1]

    # Affiche une valeur approchée de pi
    approximation = 4 * len(dans_cercle) / nb_points
    print("Pi est environ égal à : ", approximation)
    ```
        

## VI Crédits

Jean-Louis Thirot, Pierre Marquestaut, Nicolas Revéret et Mireille Coilhac

