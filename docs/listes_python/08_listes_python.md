---
author: Mireille Coilhac
title: Les listes python
---

# Les listes python



Contrairement aux tableaux que nous avons étudiés, les objets de type `list` en python ont une longueur qui peut varier.

Vous allez tester ci-dessous les différentes syntaxes à connaître en première.

## I. La méthode `append`

???+ Testez

    Création d'une liste vide

    {{IDE('scripts/liste_vide')}}


???+ Testez

    Ajout d'un élément

    {{IDE('scripts/un_ajout')}}


???+ question "À vous de jouer"

    Compléter ci-dessous : Ajouter la ligne `[7, 8, 15]` à la matrice `m` puis la faire s'afficher ligne par ligne.

    {{IDE('scripts/ajout_m')}}


??? success "Solution"

    ```python
    m.append([7, 8, 15])
    for element in m :
        print(element)
    ```

    On peut aussi procéder par concaténation de listes : 

    ```python
    m = m + [[7, 8, 15]]  # (1)
    for element in m :
        print(element)
    ```

    1. :warning: Il faut concaténer une liste avec une liste. La liste contient elle-même une liste, d'où la présence des doubles crochets.

???+ Testez

    Ajout d'éléments avec une boucle

    {{IDE('scripts/boucle_ajout')}}

## II. La méthode `index`

Renvoie la position du premier élément de la liste dont la valeur égale x (en commençant à compter les positions à partir de zéro). Une exception ValueError est levée si aucun élément n'est trouvé.

???+ Testez

    Recherche du plus petit rang d'un élément dans une liste

    {{IDE('scripts/index')}}

## III. La méthode `count`

???+ Testez

    Compter le nombre d'occurences d'un élément de la liste

    {{IDE('scripts/count')}}

## IV. La méthode `sort`

???+ Testez

    Tri croissant d'une liste. La liste est modifiée elle-même (on dit "en place")

    {{IDE('scripts/tri')}}

???+ Testez

    Tri décroissant d'une liste

    {{IDE('scripts/tri_decr')}}

## V. La fonction `sorted`

???+ Testez

    La fonction `sorted` réalise le tri croissant d'une liste. La liste elle-même **n'est pas modifiée**.
    La fonction renvoie une **nouvelle liste** triée.

    {{IDE('scripts/tri_sorted')}}


## VI. La méthode `pop`

???+ Testez

    Suppression du dernier élément de la liste

    {{IDE('scripts/pop')}}

???+ Testez

    Suppression du dernier élément de la liste et renvoi de la valeur supprimée

    {{IDE('scripts/pop_renv')}}

👉 En fait la méthode `pop` renvoie toujours la valeur supprimée, même si on ne l'affecte pas à une variable.

???+ Testez

    Suppression de l' élément de rang i de la liste 

    {{IDE('scripts/pop_i')}}

???+ Testez

    Suppression de l' élément de rang i de la liste et renvoi de la valeur supprimée

    {{IDE('scripts/pop_i_renv')}}

👉 En fait la méthode `pop` renvoie toujours la valeur supprimée, même si on ne l'affecte pas à une variable.

## VII. L'instruction `del`

Il existe un moyen de retirer un élément d'une liste à partir de sa position au lieu de sa valeur : l'instruction del. Elle diffère de la méthode pop() qui, elle, renvoie une valeur.

???+ Testez

    Suppression de l' élément de rang i de la liste 

    {{IDE('scripts/del')}}


## VIII. La méthode `remove`

Cette méthode supprime la première occurrence d'un élément. c'est pratique si on n'en connait pas l'indice.

???+ Testez

    Suppression de la première occurence d'un élément

    {{IDE('scripts/remove')}}


## IX. Tester la présence d'un élément dans une liste

Cette instruction renvoie `True` ou `False` suivant qu'un élément se trouve dans une liste ou pas.

???+ Testez

    Tester

    {{IDE('scripts/in')}}

## X. Les tranches (ou "slices" en anglais)

* Lorsque les tranches sont indiquées avec deux entiers, on prend tous les éléments de la liste de rang entier à gauche compris jusqu'au rang entier à droite exclu.
`ma_liste[a:b]` prend tous les éléments dont l'indice est un **entier** de l'intervalle **[** a, b **[**
* S'il n'y a rien avant les `:` on commence au début de la liste.
* S'il n'y a rien après les `:` on va jusqu'au bout de la liste.

???+ Testez

    les slices

    {{IDE('scripts/tranches')}}

Source : https://docs.python.org/fr/3/tutorial/datastructures.html
