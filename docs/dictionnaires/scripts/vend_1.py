# --- PYODIDE:code --- #

def vend(ferme):
    ...


# --- PYODIDE:corr --- #

def vend(ferme):
    for cle in ferme:
        ferme[cle] = ferme[cle] - 1


# --- PYODIDE:tests --- #

ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 1, "cheval": 4}
vend(ferme_gaston)

assert ferme_gaston == {"lapin": 4, "vache": 6, "cochon": 0, "cheval": 3}


# --- PYODIDE:secrets --- #

ferme_gaston_2 = {"lapin": 10, "vache": 9, "cochon": 8, "cheval": 1}
vend(ferme_gaston_2)

assert ferme_gaston_2 == {"lapin": 9, "vache": 8, "cochon": 7, "cheval": 0}