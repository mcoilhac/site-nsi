# --- PYODIDE:code --- #

def top_3_candidats(notes):
    """
    Renvoie une liste contenant les 3 meilleurs candidats
    Prend en entrée un dictionnaire de noms/notes
    """
    dico = dict(notes)  # Pour ne pas détruire le dictionnaire notes
    ...


# --- PYODIDE:corr --- #

def top_3_candidats(notes):
    """
    Renvoie une liste contenant les 3 meilleurs candidats
    Prend en entrée un dictionnaire de noms/notes
    """
    dico = dict(notes)
    top_liste = []  #Initialise une liste vide pour creer la liste des 3 meilleurs
    for i in range(3):  # On cherche les 3 premiers
        note_max = -1  # Initialisation pour entrer dans la boucle
                     # de recherche de la plus grande note
        for (nom, note) in dico.items(): #parcourt du dictionnaire
            if note > note_max:  # recherche de note maximale
                note_max = note
                premier = nom     # nom associe
        dico.pop(premier)   # supprime cet item du dictionnaire
                        # on cherche l'éleve de note maximale parmi
                        # ceux qui restent
        top_liste.append(premier) # Ajoute ce candidat à la liste des 3 vainqueurs
    return top_liste


# --- PYODIDE:tests --- #

liste_candidats = {"Candidat 7": 2, "Candidat 2": 38, "Candidat 6": 85,
                "Candidat 1" : 8, "Candidat 3" : 17, "Candidat 5" : 83, "Candidat 4" : 33}
assert top_3_candidats(liste_candidats) == ['Candidat 6', 'Candidat 5', 'Candidat 2']


# --- PYODIDE:secrets --- #

liste_candidats_2 = {"Candidat 7": 200, "Candidat 2": 38, "Candidat 6": 85,
                "Candidat 1" : 8, "Candidat 3" : 170, "Candidat 5" : 83, "Candidat 4" : 330}
assert top_3_candidats(liste_candidats_2) == ['Candidat 4', 'Candidat 7', 'Candidat 3']