Autre solution possible : 

```python title=""
def top_3_candidats(notes):
    """
    Renvoie une liste contenant les 3 meilleurs candidats
    Prend en entrée un dictionnaire de noms/notes
    """
    liste = ["", "", ""]
    top1 = 0
    top2 = 0
    top3 = 0
    for elem in notes:
        if notes[elem] > top1:
            top3, top2, top1 = top2, top1, notes[elem]
            liste[2], liste[1], liste[0] = liste[1], liste[0], elem
        elif notes[elem] > top2:
            top3, top2 = top2, notes[elem]
            liste[2], liste[1] = liste[1], elem
        elif notes[elem] > top3:
            top3 = notes[elem]
            liste[2] = elem
    return liste
```
