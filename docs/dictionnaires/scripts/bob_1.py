# --- PYODIDE:code --- #

def courses(produits, quantites):
    panier = {}
    ...


# --- PYODIDE:corr --- #

def courses(produits, quantites):
    panier = {}
    for i in range(len(produits)):
        panier[produits[i]] = quantites[i]
    return panier


# --- PYODIDE:tests --- #

assert courses(["farine", "beurre", "oeufs"], [1, 2, 12]) == {"farine": 1, "beurre": 2, "oeufs": 12}
assert courses(["pain", "confiture", "riz"], [2, 1, 3]) == {"pain": 2, "confiture": 1, "riz": 3}



# --- PYODIDE:secrets --- #

assert courses(["farine", "beurre", "oeufs"], [1, 3, 12]) == {"farine": 1, "beurre": 3, "oeufs": 12}
assert courses(["pain", "confiture", "riz"], [2, 10, 3]) == {"pain": 2, "confiture": 10, "riz": 3}
