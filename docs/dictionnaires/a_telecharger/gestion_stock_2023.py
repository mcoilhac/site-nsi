################################
## Module de gestion du stock ##
## auteur :                   ##
## date :                     ##
################################

### vous devez coder les fonctions ci-dessous ###
### ce module est utilisé par gestion_ferme.py mais pour le moment
### beaucoup de choses ne vont pas s'afficher correctement

### vous ne devez pas modifier gestion_ferme.py ###

### les fonctions renvoient des valeurs erronées
### vous devrez supprimer ces return et renvoyer les bonnes valeurs
### à la place

### pendant la mise au point de vos fonctions, il est fort possible que
### l'interface ne fonctionne plus (si par exemple une fonction renvoie None...)
### mais ne touchez pas l'autre fichier, c'est ici que ça se passe et uniquemet ici

def init_dict() :
    """
    In : rien
    Out: un dictionnaire décrivant une ferme avec 3 cochons et 7 vaches
    """
    return {}

def achat_vente(quantite, animal, ferme) :
    """
    In :
      quantité : int : quantité à vendre/acheter (positif si achat / négatif si vente)
      animal : str : animal a vendre/acheter
      ferme : dict : dictionnaire décrivant la ferme
    Out:

       Si quantité est négatif : opération de vente
           - vérifier que le stock de l'animal est suffisant
           si oui :
               - modifier le stock
               - écrire en console "Vente effectuée"
               - si le stock pour cet animal est 0, supprimer la clef dans le dict
               - renvoyer 0 (pour indiquer qu'il n'y a pas d'erreur)
           si non :
               - ne rien modifier
               - écrire en console "Vente impossible"
               - renvoyer 1 (pour indiquer l'erreur stock insuffisant)

        Si quantité est positif : opération d'achat
           - vérifier si l'animal est déjà dans la ferme
           si oui :
               - modifier le stock,
               - écrire en console "Achat effectué"
               - renvoyer 0
           si non :
               - créer une clef, avec la bonne valeur
               - ecrire en console "Achat effectué"
               - renvoyer 0
    """
    return 0

def nb_especes(ferme) :
    """
    In : le dictionnaire de la ferme
    Out : le nombre d'espèces

    Exemple : si ferme = {"vache": 3, "cochon": 4, "poulet": 5}
    la fonction devra renvoyer 3

    """
    return 0

def especes(ferme) :
    """
    In : le dictionnaire de la ferme
    Out: str : la fonction renvoie une chaine avec les animaux, séparés par " / "

    Exemple : si ferme = {"vache": 3, "cochon": 4, "poulet": 5}
    la fonction devra renvoyer "vache / cochon / poulet"

    """
    return ""

def nb_total_animaux(ferme) :
    """
    In : le dictionnaire de la ferme
    Out: int : la fonction renvoie le nombre total d'animaux

    Exemple : si ferme = {"vache": 3, "cochon": 4, "poulet": 5}
    la fonction devra renvoyer 12

    """
    return 0
