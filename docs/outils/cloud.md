---
author: Mireille Coilhac
title: Le cloud
--- 

Suivre le lien : [https://classroom.google.com](https://classroom.google.com){ .md-button target="_blank" rel="noopener" }

## I. Première connexion si vous êtes déjà connecté sur un compte gmail

En haut à cliquer sur l’icône du compte sur lequel vous êtes connecté : 

![compte](images/compte.png){ width=20% }


Puis cliquer sur : ![ajouter](images/ajouter.png){ width=20% }

Puis cliquer sur : ![utiliser](images/utiliser.png){ width=20% }

Saisir ici votre adresse Classroom donnée par le lycée qui est de la forme p.nom@classestaspais.fr : 

![connexion](images/connexion.png){ width=30% }

Puis Saisissez votre mot de passe fourni par le lycée : ![mdp](images/mdp.png){ width=20% }

## II. Première connexion si vous n’êtes pas connecté sur un compte gmail

Cliquer sur Utiliser un autre compte: ![autre](images/autre.png){ width=20% }

Saisir votre adresse Classroom donnée par le lycée qui est de la forme p.nom@classestaspais.fr : 

![connexion_2](images/connexion_2.png){ width=20% }

Puis Saisissez votre mot de passe fourni par le lycée : ![mdp](images/mdp.png){ width=20% }

## III. Pour se connecter les fois suivantes : 

Suivre le lien : [https://classroom.google.com](https://classroom.google.com){ .md-button target="_blank" rel="noopener" }

En haut à cliquer sur l’icône du compte sur lequel vous êtes connecté : 

![compte](images/compte.png){ width=20% }

Sélectionner votre compte qui se termine par @classestaspais.fr

Si besoin, suivre les indications.

## IV. Créer votre Cloud et vos premiers dossiers

![cloud](images/cloud.png){ width=80% }

Cliquer ici : 

![apps](images/apps.png){ width=20% }

Cliquer sur Drive :  

![apps](images/drive.png){ width=20% }

Cliquer sur + Nouveau : 

![nouveau](images/nouveau.png){ width=20% }

Cliquer sur Dossier : 

![dossier](images/dossier.png){ width=20% }

Créer le dossier NSI première :   
👉 Compléter le nom du dossier puis cliquer sur créer ![creer](images/creer.png){ width=20% }

Vous voyez apparaître votre nouveau dossier : ![nsi](images/nsi.png){ width=20% }

Sélectionner ce dossier NSI première : ![selection](images/selection.png){ width=20% }

* Puis recommencer la procédure (cliquer un nouveau, puis dossier etc …) pour créer le dossier Python.
* Se replacer sur le dossier NSI première, puis recommencer la procédure (cliquer un nouveau, puis dossier etc …) pour créer le dossier Encodage.
* Se replacer encore sur le dossier NSI première, puis recommencer la procédure (cliquer un nouveau, puis dossier etc …) pour créer le dossier Algèbre de Boole.

Vous devez obtenir ceci :  

![dossiers](images/dossiers.png){ width=80% }

Ou tout simplement ceci si la fenêtre sur les raccourcis a été fermée :

![dossiers_2](images/dossiers_2.png){ width=80% }

## V. Déposer vos fichiers sur le Cloud

Cliquer sur le dossier voulu, par exemple Python : 

![voulu](images/voulu.png){ width=50% }

Suivre les instructions : 

![deposer](images/deposer.png){ width=20% }

## VI. Récupérer un fichier se trouvant sur le Cloud : 

Aller dans le dossier désiré, puis faire un clic droit sur le fichier voulu.  
👉 Cliquer sur Télécharger 

![telecharger](images/telecharger.png){ width=20% }

!!! info "Le menu"

    Vous observez que ce menu vous permet de faire pas mal de choses.  
    A vous de suivre les instructions si vous en avez besoin.



