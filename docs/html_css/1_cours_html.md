---
author: Fabrice Nativel, Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac
title: HTML
---
 
## I. Présentation

### Modèle client-serveur
 
<div class="centre"><iframe src="https://player.vimeo.com/video/138623558?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>

### Page WEB
<div class="centre">
<iframe src="https://player.vimeo.com/video/138623756?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>

### HTML, CSS et JS
<div class="centre"><iframe src="https://player.vimeo.com/video/138623826?color=b50067&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe></div>

!!! abstract "Contenu et style"

    &#128083; Une page **HTML** est écrite en caractères lisibles mais codée.  
    Il n'est pas question d'étudier l'ensemble des possibilités du langage HTML, seulement les bases.

    &#128087;&#128086; / &#128221; On sépare le **style**(mise en page, couleurs, taille, type de polices de caractère, couleur de fond de page, ou des blocs,
    etc... du **contenu**.  
    &#128073; Le style est généralement défini dans un fichier séparé (ce n'est pas obligatoire, mais c'est très recommandé, et c'est ainsi que nous allons travailler).

    &#127797; Encore faut-il bien comprendre ce qu'on entend par le style et le contenu ! 
   


## II. Structure générale d'une page HTML

!!! abstract "Un début"

    ```html 
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
        </body>
    </html>
    ```


!!! abstract "Structure"

    Un document **HTML** est constitué d'un **document** contenant une **en-tête**, suivi d'un **corps**. 

    Repérer les balises permettant de délimiter :  

    * Le document

    ??? success "Solution"

        ```html
        <html>
        </html>
        ```

    * L'en-tête

    ??? success "Solution"

        ```html
        <head>
        </head>
        ```

    * Le corps

    ??? success "Solution"

        ```html
        <body>
        </body>
        ```

## III. Un premier exemple : 

Suivre ce lien, puis recopier dans la partie HTML le code ci-dessous.
Pour visualiser le rendu, cliquer sur Run.

[jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

!!! example "Exemple à recopier"

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
            <h1>Titre de niveau 1</h1>
                <p>Un paragraphe d'introduction de la section</p>
            <h2>Titre de niveau 2</h2>
                <p>
                    Un premier paragraphe dans cette partie....
                </p>
                <p>
                       Puis un second paragraphe. 
                       Il peut évidement y en avoir plusieurs.
                </p>         
        </body>
        </html>
        ```

Nous allons étudier ceci plus en détail.

😥 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." n'est pas écrite à la ligne comme sur le code.

👉 HTML ne prend pas en compte les espaces ou les sauts de lignes. Pour créer un saut de ligne, il faudra par exemple utiliser une balise `<br>`

## IV. Le concept de balises ouvrantes et fermantes, ou orphelines

### 1. Balises en paires

!!! abstract "Blocs"

    Comme vous pouvez commencer à le voir, dans l'exemple ci-dessus, une page <b>html</b> est organisée en blocs, 
    imbriqués les uns dans les autres. Le <b class="blue">header</b> est inclus dans le <b class="blue">html</b >, et le <b class="blue">title</b> est inclus dans
    le <b class="blue">header</b>


    Les blocs sont délimités par deux **balises**.  
    Par exemple :
    
    * **`<head>`** indique le début du header. C'est une **balise ouvrante**.
    * **`</head>`** indique la fin du header. C'est une **balise fermante**.


!!! abstract "Paires"

    On parle ici de balises en paire, avec une <b>balise ouvrante</b> et une <b>balise fermante</b>. 

!!! warning "&#128546; Et si on oublie une balise ?"

    
    Les navigateurs sont en général très tolérants, et une balise incorrecte (par exemple, il manque une fermante, ou il y a une fermante mais pas d'ouvrante...)
    sera ignorée. Dans les cas des balises ouvrantes sans fermante, cela engendrera le plus souvent un affichage incorrect et peu lisible. Dans
    le cas des balises fermantes sans ouvrante, l'effet est en général simplement nul : le navigateur ignore cette balise qui n'est 
    pas compréhensible. Toutefois, certains navigateurs, ou certaines balises, provoqueront des affichages totalement différents
    de ce qui est attendu et très souvent illisibles. C'est la responsabilité de l'auteur du document de veiller à respecter la syntaxe 
    du html.

??? tip "Astuce"

    &#128161; Il est conseillé, lorsqu'on écrit une balise ouvrante, d'instantanément écrire la balise fermante correspondante, et d'écrire ce que l'on souhaite entre les deux.

    😊 Pour s'y retrouver, il est recommandé d'utiliser des indentations.

!!! danger "Important"

    &#128073; Toutes ces balises indiquent **seulement** ce que contiennent les blocs. 
    Cela ne précise en aucun cas comment il faut les afficher dans la page. 
    Toutes les indications concernant **le style** seront
    fournies dans un fichier à part : la feuille de style **CSS**


### 2. Balises orphelines

Certaines balises ne servent pas à désigner un contenu, mais servent à ajouter un élément.

!!! example ""Exemples"

    * <b class="blue">&lt;br></b> indique un saut de ligne.  
    * La balise <b class="blue">&lt;hr></b> introduit une ligne horizontale dans toute la largeur de la page.  
    * <b class="blue">La balise &lt;img></b> qui sert à insérer une image est également une <b>orpheline</b>, mais 
    son utilisation est un peu différente et nous en reparlerons.

!!! example "Exemple à recopier"

    Lien pour visualiser : [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>
            <h1>I. Titre de niveau 1</h1>
                <p>Un paragraphe d'introduction de la section</p>
            <h2>Titre de niveau 2</h2>
                <p>
                    Un premier paragraphe dans cette partie....
                </p>
                <p>
                       Puis un second paragraphe. 
                       <br> Il peut évidement y en avoir plusieurs.
                </p> 
                <hr>
                <h1>II. Titre de niveau 1 : </h1>
                    
        </body>
        </html>
        ```

😊 Vous pouvez déjà observer que la phrase "Il peut évidemment ..." est bien écrite avec un saut de ligne grâce à la balise `<br>`


### 3. Quelques balises usuelles 

Aller sur ce site : [jsfiddle.net](https://jsfiddle.net/){ .md-button target="_blank" rel="noopener" }

#### a) Ecrire un nouveau document HTML complet. Dans le corps du document, c'est à dire entre les balises `<body>` et `</body>`, insérer le contenu suivant :

!!! abstract "Ma recette de cuisine"

    ```html linenums="1"
    <h1> La recette du carry de poulet </h1>

        <h2> Les ingrédients </h2>
            <ul>
                <li> un poulet découpé en morceaux </li>
                <li> 3 oignons </li>
                <li> 1 tomate </li>
                <li> 5 gousses d'ail </li>
            </ul>
        
        <h2> La préparation </h2>
            <p>Dans de l'huile chaude, faire revenir le poulet</p>
    ```

    ??? success "Solution"

        ```html 
        <!DOCTYPE html>
        <html>
            <head>
                <title>Titre de la page</title>
            </head>


            <body>
                <h1> La recette du carry de poulet </h1>

                <h2> Les ingrédients </h2>
                    <ul>
                        <li> un poulet découpé en morceaux </li>
                        <li> 3 oignons </li>
                        <li> 1 tomate </li>
                        <li> 5 gousses d'ail </li>
                    </ul>
        
                <h2> La préparation </h2>
                    <p>Dans de l'huile chaude, faire revenir le poulet</p>
            </body>
        </html>
        ```


#### b) Observer le résultat obtenu dans l'affichage et en déduire le rôle des balises suivantes :  

* `<ul>` et `</ul>`
* `<li>` et `</li>`

??? success "Solution"

    * `<ul>` représente une liste d'éléments non numérotés. Elle est souvent représenté par une liste à puces.
    * `<li>` est utilisé pour représenter un élément dans une liste. Celui-ci doit être contenu dans un élément parent : une liste ordonnée (`<ol>`), une liste non ordonnée (`<ul>`) ou un menu (`<menu>`). 

#### c) Faites les modifications suivantes :

* Ajouter un sous-titre 'Accompagnements' (titre de niveau 2) dans la page Web.
* Dans le sous-titre créer une liste à puces avec deux éléments : "riz blanc et grains", "riz jaune".
* Ajouter un paragraphe au début de la recette dans lequel on écrira "Le carry de poulet est une recette de cuisine traditionnelle de l'île de la Réunion"

??? success "Solution"

    ```html
    <!DOCTYPE html>
    <html>
        <head>
            <title>Titre de la page</title>
        </head>


        <body>

            <h1> La recette du carry de poulet</h1>

                <p>
                    Le carry de poulet est une recette de cuisine traditionnelle de l'île de la Réunion
                </p>

            <h2> Les ingrédients </h2>
                <ul>
                    <li> un poulet découpé en morceaux </li>
                    <li> 3 oignons </li>
                    <li> 1 tomate </li>
                    <li> 5 gousses d'ail </li>
                </ul>

            <h2> La préparation </h2>
                <p> Dans de l'huile chaude, faire revenir le poulet</p>

            <h2> Accompagnements</h2>
                <ul>
                    <li> riz blanc et grains </li>
                    <li> riz jaune </li>
                </ul>
        </body>
    </html>


    ```

* Ajouter le paragraphe suivant : 

??? success "A ajouter"

    ```html
    <p>
    <a href="https://fr.wikipedia.org/wiki/La_R%C3%A9union">île de la Réunion</a>
    </p>
    ```

Que s'est-il passé ?

??? success "Solution"

    Si l'on clique sur "île de la Réunion", on ouvre le lien donné sur l'île de la réunion.

* Ajouter un lien dans votre page Web sur les mots "3 oignons" qui permet d'accéder à l'adresse :  
 `https://fr.wikipedia.org/wiki/Oignon`

??? success "Solution"

    ```html
    <li> <a href=" https://fr.wikipedia.org/wiki/Oignon">3 oignons</a> </li>
    ```

!!! Important
    Les balises HTML permettent de **structurer** le contenu d'une page web, en définissant les titres, les paragraphes, ...

    &#127797; Pour modifier l'**apparence** d'une page, on a recours au **css** (**c**ascadind **s**tyle **s**heet) qui permet de modifier l'apparence du contenu de la page.
    
    😊 Nous verrons cela dans la leçon suivante.

## V. Insérer un lien

Nous avons vu un premier exemple avec la recette de cuisine.

!!! info "Insérer un lien"

    Voyons deux types de liens :

    * Les liens vers une pages externe :  
    Par exemple :  
    `<a href="https://fr.wikipedia.org/wiki/Anatomie_des_l%C3%A9pidopt%C3%A8res"> anatomie des lépidoptères.</a>`.

    * Les liens vers une autre page du même site :  
    Par exemple  :  
    `<a href="page2.html">Les lépidoptères</a>`  
    Dans ce cas le fichier peut être indiqué par un chemin relatif ou absolu. Nous verrons un exemple dans le paragraphe VII.


## VI. Insérer une image


!!! info "Insérer une image"

    La syntaxe pour insérer une image est la suivante :

    `<img src="source de l'image" alt="descritption de l'image">`

    👉 Les deux attributs src et alt sont obligatoires dans la norme HTML5.

!!! info "Source de l'image"

    Le source de l'image peut être :

    * Un nom de fichier : papillon.jpg. Dans ce cas le fichier contenant l'image doit se trouver dans le même répertoire que le fichier html
    * Un nom de fichier avec chemin relatif : ../images/papillon.jpg.
    * Un nom de fichier avec chemin absolu : /images/papillon.jpg. Dans ce cas, le fichier contenant l'image doit se trouver à l'endroit spécifié en partant de la racine du site.
    * Une adresse web : http://data.ba-bamail.com/Images/2014/9/10/934e8182-ea3f-4b3d-b1bf-dbda4fba9c3c.jpg

    👉 La première image est un fichier image, enregistré dans le même dossier que la page html. La seconde image est une image située sur le web, en l’occurrence, dans une page de wikipédia.

    👉 Notez bien qu'à ce stade on ne s'intéresse qu'au contenu. La façon dont l'image doit être présentée sera traitée ailleurs (centrée ou non, dans le texte ou séparée du texte, taille de l'image, etc..). Toutefois, si on veux des retours à la ligne avant et après l'image, on pourra placer la balise <img..> dans un paragraphe, comme ceci :

    ```html
    <p>
        <img src="image_locale.jpg" alt="une chenille">
    </p>
    ```

???+ note dépliée "Les chemins"

    Si l'image n'est pas dans le même dossier que le fichier html, il faudra utiliser un "chemin".

    👉 Voir le paragraphe "compléments"

## VII. Ecrire et visualiser une page écrite en HTML

!!! info "Editeur de code HTML"

    Il existe de nombreux éditeurs de code HTML, comme Notepad++, ou Sublime Text. Ils vous aideront à ne pas faire de faute de syntaxe.

???+ question "Créer un fichier HTML"

    Ouvrir l'éditeur dont vous disposez, copiez-collez le code ci-dessous, puis enregistrez votre fichier sous le nom : `essai_HTML.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <title>Apprendre</title>
        </head>


        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```



!!! info "Visualiser du code HTML"

    * Pour le moment, nous avons utilisé le site en ligne `https://jsfiddle.net/`, très pratique pour de petits essais.  
    * 👉 A partir de maintenant nous allons tout simplement ouvrir un fichier HTML avec un navigateur.


???+ question "Visualiser un fichier HTML"

    * Utiliser votre explorateur de fichier, pour retrouver votre fichier.
    * Faire un clic droit sur votre fichier, puis "ouvrir avec", puis sélectionner "Firefox", ou "Google Chrome", ou "Microsoft Edge" par exemple.
    * Faire de même avec "Internet explorer" s'il est encore installé sur votre ordinateur.

    Que se passe-t-il ?

    ??? success "Vous pouvez obtenir quelque chose qui ressemble à ceci :"

        * Avec un navigateur "récent" : 

        ![avec Firefox](images/firefox.png){ width=35% .center}

        * Avec Internet Explorer : 

        ![avec Internet Explorer](images/intern_explor.png){ width=35% .center}


!!! info "Importance du `<head>`"

    😥 Comme vous le voyez, les navigateurs n'affichent pas toujours la même chose. C'est pourtant le même ficher, mais chaque navigateur peut interpréter les choses à sa façon.
    
    Ici c'est un problème d'encodage des accents qui provoque cette différence, et pour assurer un affichage correct dans tous les navigateurs, il faut ajouter une information dans le head.

    👉 Nous ajouterons dorénavant toujours dans `<head>` cette ligne : `<meta charset="UTF-8">`.

    😊 Ceci nous évitera le problème des accents, nous indiquons au navigateur que le texte est codé dans la norme utf-8 qui permet d'intégrer des accents dans le texte.


???+ question "Visualiser un fichier HTML avec le `head` complété"

    Recommencer l'expérience précédante avec le code suivant : 

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Apprendre</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Il existe plusieurs façons de s'entraîner.</p>       
        </body>
        </html>
        ```


## VIII. Un site à plusieurs pages

Nous allons voir comment cela fonctionne avec un site composé d'une page d'accueil et de deux autres pages.

!!! info "index"

    Il faut commencer par créer la page **obligatoirement** nommée `index.html`

    Voici un exemple plus détaillé que ce que nous avons déjà étudié.  
    Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `index.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE html>
        <html lang="fr">
            <head>
                <meta charset="utf-8">
                <title> Home </title>
            </head>
            <body>
                <header>
                    <h1> Mon en-tête : Accueil </h1>
                    <nav> 
                        <a href="page1.html"> page 1</a>
                        <br>
                        <a href="page2.html"> page 2</a>
                    </nav>
                </header>
                <main>
                    <section>
                    <h2> Section 1 </h2>
                    <p> Bla bla bla </p>
                    </section>

                    <section>
                    <h2> Section 2 </h2>
                    <p> Blo blo blo </p>
                    </section>
                </main>
                <footer> 
                    Mon pied de page
                </footer>
            </body>
        </html>
        ```


!!! info "Page 1"

    Vous devez maintenant écrire votre page 1.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page1.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 1</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 1</p>       
        </body>
        </html>
        ```


!!! info "Page 2 "

    Vous devez maintenant écrire votre page 2.  
    Voici un exemple. Le recopier, et grâce à votre éditeur l'enregistrer sous le nom `page2.html`

    ??? success "Code à recopier"

        ```html
        <!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="UTF-8">
            <title>Page 2</title>
        </head>

        <body>
            <h1>I. Généralités</h1>
                <p>Je suis sur la page 2</p>       
        </body>
        </html>
        ```

!!! info "Visualiser votre tout petit site"

    Les trois fichiers doivent se trouver dans le même dossier.  
    Ouvrir le fichier `index.html` avec votre navigateur.
