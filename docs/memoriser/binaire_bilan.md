---
author: Mireille Coilhac
title: Binaire - Bilan
---

## Prérequis sur les puissances

![puissances](images/regles_puissances.jpg){ width=15%; : .center }

{{ multi_qcm(
    [
        "$2^3=$",
        [
            "9",
            "8",
            "Je ne sais pas",
            "6",
        ],
        [2],
    ],
    [
        "$3^2=$",
        [
            "9",
            "8",
            "Je ne sais pas",
            "6",
        ],
        [1],
    ],
    [
        "$2^0=$",
        [
            "0",
            "1",
            "Impossible",
            "2",
        ],
        [2],
    ],
    [
        "$2^1=$",
        [
            "0",
            "1",
            "Je ne sais pas",
            "2",
        ],
        [4],
    ],
    [
        "$0^3=$",
        [
            "0",
            "1",
            "Impossible",
            "3",
        ],
        [1],
    ],
    [
        "$16^3 \\times 16^2=$",
        [
            "$16^5$",
            "$16^6$",
            "Je ne sais pas",
            "2",
        ],
        [1],
    ],
    [
        "$16^0\\times 16^2=$",
        [
            "$16^0",
            "$16^2",
            "Je ne sais pas",
            "16",
            "1",
        ],
        [2],
    ],
    [
        "$2^3+2^4=$",
        [
            "$2^7$",
            "$2^{12}$",
            "Je ne sais pas",
            "24",
        ],
        [4],
    ],
    multi = False,
    qcm_title = "QCM à refaire tant que nécessaire ...",
    DEBUG = False,
    shuffle = True,
    ) }}


## Les bases numériques


!!! info "Numération de position"

	C’est celle que nous utilisons actuellement tous les jours :

	$2023 = 2 \times 1000 + 0 \times 100 + 2 \times 10 + 3 \times 1$.

	Ou encore : 2023 est égal à 2 **milliers**, 0 **centaines**, 2 **dizaines**, 3 **unités**.

	$2023 = 2 \times 10^3 + 0 \times 10^2 + 2 \times 10^1 + 3 \times 10^0$.


### Lire un nombre

!!! example "un exemple en base 10"

	$12734 = 4 \times 10^0 + 3 \times 10^1 + 7 \times 10^2 + 2 \times 10^3 + 1 \times 10^4$


!!! example "un exemple en base 2"

	$(101010)_2 = 0 \times 2^0 + 1 \times 2^1 + 0 \times 2^2 + 1 \times 2^3 + 0 \times 2^4 + 1 \times 2^5 = 0 + 2 + 0+ 8 + 0 + 32  =  (42)_{10}$


😊 Pas si difficile ! Vous avez appris à convertir un nombre d'une base N vers la base 10.


!!! info "Notations"

	Pour préciser dans quelle base sont écrits les nombres, on peut utiliser des parenthèses : 

	$(42)_{10}=(101010)_2$

	Autres notations possibles :$(101010)_2 = 101010 _{(2)}= 0\text{b}101010 = 101010\text{b}$


### Compter en binaire

!!! example "Quelques exemples"

	$(1)_2+(1)_2=(2^0  + 2^0)_{10}=(2 \times 2^0)_{10} =(2^1)_{10}= (10)_2$

	$(10)_2 +(1)_2=(2^1+2^0)_{10}=(11)_2$

	$(11)_2+(1)_2= (2^1+2^0+2^0)_{10}=  (2^1+2 \times 2^0)_{10}=  (2^1+2^1)_{10}=(2 \times 2^1)_{10}=(2^2)_{10}=(100)_2$


!!! info "Comptons en base 2"

	Compter, c'est ... ajouter 1 😊 

	0; 1; 10; 11; 100; 101; 110; ...


???+ question "Si on comptait ?"

    Trouver les entiers qui suivent ceux-ci lorsque l'on compte : 0; 1; 10; 11; 100; 101; 110;

    ??? success "Solution"

    	111; 1000; 1001; 1010; 1011; 1100; 1101; 1110; 1111; 10000; 10001; 10010 ...


???+ note dépliée "Remarquer en décimal"

	* 99 + 1 = 100 
	* 999 + 1 = 1000
	* 9999 + 1 = 10000 …

### De la base 10 vers la base 2

!!! info "Poser les divisions _à la main_"

	Petit retour sur les divisions dites "euclidiennes"

	![div_eucl](images/div_eucl.png){ width=20%; : .center }

	> Source de l'image : [🌐 Division euclidienne](https://www.jeuxmaths.fr/cours/division-euclidienne.php){:target="_blank" }


!!! info "Méthode par divisions successives"

	🤔 Etudier l'exemple ci-dessous

	👉 On poursuit les divisions jusqu'à obtenir un quotient **égal à 0** pour la dernière division.  
	On lit ensuite les restes, en partant du bas.

	![decimal_binaire](images/decimal_binaire.png){ width=30%; : .center }

	> Source de l'image : [🌐 Académie de Limoges](http://pedagogie.ac-limoges.fr/sti_si/accueil/FichesConnaissances/Sequence2SSi/co/ConvDecimalBinaire.html){:target="_blank" }



## Exercices

???+ question "Exercice 1"

    1110010 est écrit en base 2. L'écrire en base 10.

    ??? success "Solution"

		$(1110010)_2 = 0 \times 2^0 + 1 \times 2^1 + 0 \times 2^2 + 0 \times 2^3 + 1\times 2^4 + 1 \times 2^5 + 1 \times 2^6 = 2 + 16 + 32 + 64  =  (114)_{10}$

    	1110010 en binaire s'écrit 114 en décimal.



???+ question "Exercice 2"

    convertir 23 écrit en décimal en binaire

    ??? success "Solution"

    	10111

    	![23](images/div_succ.jpg){ width=30%; : .center }


???+ question "Exercice 3"

    234 est écrit en base 10. L'écrire en binaire

    ??? success "Solution"

    	Le nombre 234 écrit en base dix, s'écrit 11101010 en base deux .



???+ question "Exercice 4"

	On peut aussi convertir un nombre décimal en binaire en utilisant un tableau.

	[Utiliser un tableau](https://wims.math.cnrs.fr/wims/wims.cgi?lang=fr&cmd=new&module=H3%2Fcoding%2Foefbin.fr&exo=binary&qnum=1&scoredelay=&seedrepeat=0&qcmlevel=1&special_parm2=&special_parm4=){ .md-button target="_blank" rel="noopener" }

	

???+ question "Exercice 5 : A vous de jouer !"

	Imaginez vos propres exercices, et vérifiez la réponse ci-dessous.

	[Convertisseur WIMS](https://wims.math.cnrs.fr/wims/wims.cgi?module=tool/number/baseconv.en){ .md-button target="_blank" rel="noopener" }


## Des opérations en binaire

### Les additions

!!! info "Rappel en décimal"

    ![addition décimale](images/addition_dec.png){ width=20% }

!!! example "Exemples en binaire"

    $1100110_2+1111101_2 = ?$

    ![addition 1](images/addition_1.png){ width=30% }

    $1111111_2+1_2 = ?$

    ![addition 2](images/add_bin.jpg){ width=30% }

    On a donc $1111111_2+1_2 =  100000000_2$

	👉 C'est important à remarquer, et cela se généralise évidemment pour tous les nombres constitués uniquement de 1 auxquels on ajoute 1.




### Les multiplications

!!! info "💚 A noter"

    Quand on multiplie en binaire par **2** on écrit un **0** à la droite du nombre, comme pour la multiplication par **10** en décimal.


## Les bits et les octets

???+ question "Exercice 1"

    Pour coder tous les nombres entiers de 1 à 1000, combien de bits faut-il ?

    ??? success "Solution"

        $(1000)_{10}=(1111101000)_2$

        Il faut donc 10 bits.

!!! info "Les octets"

	Dans la mémoire de l'ordinateur les informations sont codées dans des octets. C'est la plus petite unité d'information qu'on peut lire/écrire. Un octet correspond à 8 bits.

	👉 Un octet permet donc de coder $2^8=256$ valeurs différentes.

