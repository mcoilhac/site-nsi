---
author: Mireille Coilhac
title: Types de variables - À vous
---

## Types de base

|Type|Description|Exemples
|:--|:--|:--|
|$\hspace{10em}$|$\hspace{15em}$|`ma_variable = 3`|
|$\hspace{10em}$|$\hspace{15em}$|`ma_variable = 2.5`|
|$\hspace{10em}$|$\hspace{15em}$|`ma_variable = "NSI"`|
|$\hspace{10em}$|$\hspace{15em}$|`ma_variable = True`|


!!! warning "Remarque"

    Nous étudierons plus tard dans l'année le type liste Python `list`, et le type dictionnaire `dict`

!!! info "Les flottants"

	1 est un entier, et 1.0 est un `float`. Ils sont égaux (ces deux écritures représentent la même valeur) mais pas identiques :

	```pycon
	>>> 1 == 1.0
	...
	>>> 1 is 1.0
	...
	>>> type(1)
	<class 'int'>
	>>> type(1.0)
	<class 'float'>
	```



## Conversion d'un type à un autre

!!! info "Les transtypages"

	* On peut convertir des `float` en `int`, ou inversement. 

	* On peut également convertir des chaînes `str` en `int` ou en `float` mais seulement si la chaîne contient un nombre compréhensible.

	```python
	a = int(1.23)   # a vaudra ...
	a = float(1)    # a vaudra ...
	a = int("12")   # a vaudra ...
	chaine = str(3) # chaine vaudra ...
	```

	😰 **Mais**

	```python
	a = int("douze")  #              
	a = int("12.3")   # 
	a = int("1.0")    # 
	```



