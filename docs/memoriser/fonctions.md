---
author: Mireille Coilhac
title: Fonctions - Bilan
---  

!!! info "Définition d'une fonction"    

    ```python
    def prix(nbre_adultes, nbre_enfants):  # (1)                
        resultat = 37 * nbre_adultes + 28 * nbre_enfants  
        return resultat  # (2) 

    prix(3, 2)  # (3)                                
    ``` 

    1. Une fonction commence par le mot clef <span style="color:blue;">def</span>, suivie du **nom** de la fonction, puis entre parenthèse, les **paramètres** de la fonction.

    2. `return ` signifie **renvoyer**, c'est cette ligne qui indique **ce que va renvoyer la fonction**. 
        Ici elle renvoie resultat, donc elle renvoie **la valeur de la variable resultat**

    3. Cette ligne de code ne fait pas partie de la fonction (Elle n'est pas dans le bloc indenté). Les lignes de codes qui ne sont pas dans une fonction font partie de ce qu'on appelle le **programme principal**.  
    Une fois qu'une fonction est définie, il est possible de l'appeler (l'utiliser) dans le programme principal, mais aussi dans une autre fonction.

    !!! warning "Prenez le temps de lire les commentaires (cliquez sur les +)"


!!! info "Utiliser le résultat renvoyé par une fonction"
       
    Toutefois, l'appel ci-dessus ne fait qu'appeler la fonction, qui **renvoie** le résultat. Ceci ne sera pas utile si nous ne conservons pas ce résultat (afin de pouvoir l'utiliser dans la suite du programme). Pour cela nous allons en général **affecter** ce résultat à une variable :
        
    ```python
    prix_a_payer = prix(3, 2)
    print("le prix à payer : ", prix_a_payer) 
    ```

    Si on veut seulement afficher le résultat, on peut directement afficher ainsi :

    ```python
    print("le prix à payer : ", prix(3, 2)) 
    ```

!!! info "Arguments de la fonction" 
    
    Quand on **« appelle »** `prix(3, 2)` :   
    •	3 est automatiquement affecté au 1er paramètre : la variable  `nbre_adultes`  
    •	2 est automatiquement affecté au second paramètre : la variable  `nbre_enfants`

    3 et 2 sont les valeurs données en arguments.
    
### Remarque 1 : 

???+ note dépliée "Une fonction sans paramètres"

    Certaines fonctions n'ont aucun paramètre. Dans ce cas, on met des parenthèses vides aussi bien dans la définition que dans l'appel de la fonction :
    
    ```python
    def ma_fonction() :
        instruction du bloc
    
    ma_variable  = ma_fonction()
    ```

    
### Remarque 2 : 

???+ note dépliée "Des fonctions qui ne renvoient rien"

    Certaines fonctions ne renvoient rien.  
    
    Exemple :
    
    ```python
    def ma_fonction(nom) :
        print("Votre nom est :",nom)
        return None # ou simplement return, ou pas de return du tout ...
    
    ma_fonction()
    ```
    Cette fonction **ne renvoie rien** 
        
    De telles fonctions sont, dans certains langages, appelées des procédures. En python, on ne fait pas de différence. Une procédure **fait quelques chose** : ici par exemple, elle sert à afficher (dans la console) un message.
        
    Notez que dans l'appel d'une procédure, on n'affecte pas le résultat à une variable. C'est logique car il n'y a pas de résultat, puisque la fonction ne renvoie rien.

### Notion d'espace de noms
    

!!! note "Définitions :heart:"

    - Les variables définies dans le corps d'une fonction sont appelées **variables locales**.
    - Les variables définies dans le corps du programme (sous-entendu : pas à l'intérieur d'une fonction) sont appelées **variables globales**.


!!! note "Règles d'accès aux variables locales et globales :heart:"

    - **règle 1 :** une **variable locale** (définie au cœur d'une fonction) est **inaccessible** hors de cette fonction.
    - **règle 2 :** une **variable globale** (définie à l'extérieur d'une fonction) est **accessible** en **lecture** à l'intérieur d'une fonction.
    - **règle 3 :** une **variable globale** (définie à l'extérieur d'une fonction) **ne peut pas être modifiée** à l'intérieur d'une fonction.


![global_regles.png](../Python/images/global_regles.png){ width=80%; : .center }
> Source : Gilles Lassus



!!! info "Testez vos fonctions"
         
    Exemple d'utilisation pour une fonction `carre` qui élève au carré: 
        
    ```python
        assert carre(3) == 9, "l'appel carre(3) devrait renvoyer 9"
    ```    

    Si le test est réussi, il ne se passera rien, sinon le code **lève une exception AssertionError** et affiche le message.
        
    👉 Remarque : souvent nous n'écrirons pas de message explicatif. Nous nous contenterons par exemple de : 
        
    ```python
        assert carre(3) == 9
    ```  
        
    Dans ce cas-là, si le test est réussi, il ne se passera rien, sinon le code **lève une exception AssertionError** et n'affiche pas de message.

!!! info "Une fonction peut appeler une autre fonction"

    Toutes nos fonctions (que nous écrivons ou que nous importons) sont définies avant toute exécution du programme. De sorte que, lorsque l'exécution du code commence, elles sont toutes reconnues et utilisables en tout point du code, y compris dans d'autres fonctions. 
    



