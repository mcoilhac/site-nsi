# --- PYODIDE:env --- #

import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
plt = PyodidePlot('cible_1')  
plt.target()


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

from timeit import timeit

def tri_insertion(tableau):
    for i in range(1, len(tableau)):
        valeur_a_inserer = tableau[i]
        j = i
        while j > 0 and tableau[j - 1] > valeur_a_inserer:
            tableau[j] = tableau[j - 1]
            j = j - 1
        tableau[j] = valeur_a_inserer

abscisse = [100, 200, 400, 800, 1600, 2200]
ordonnee = []

for taille in abscisse :
    liste_decr = [i for i in range(taille, 1, -1)] # création de la liste de taille n : [n, n-1, ..., 1]
    ordonnee.append(timeit("tri_insertion(list(liste_decr))", number = 10, globals = globals()))

plt.plot(abscisse, ordonnee, "ro-", label = "Tri insertion") # en rouge
plt.legend()
print("C'est bon !")
plt.show()
