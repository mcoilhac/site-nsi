# --- PYODIDE:env --- #

import matplotlib         # Indispensable (provoque la déclaration de PyodidePlot)
plt = PyodidePlot('cible_1')  
plt.target()


# --- PYODIDE:code --- #
# L'import suivant a été fait dans du code caché : 
# import matplotlib.pyplot as plt

from timeit import timeit


def tri_selection(tableau):
    for i in range(len(tableau) - 1):
        i_mini = i
        for j in range(i + 1, len(tableau)):
            if tableau[j] < tableau[i_mini]:
                i_mini = j
        tableau[i], tableau[i_mini] = tableau[i_mini], tableau[i]

abscisse = [100, 200, 400, 800, 1600, 2200]
ordonnee = []

for taille in abscisse :
    liste_cr = [i for i in range(taille)] # création de la liste de taille n : [0, 1, ...,n-1]
    ordonnee.append(timeit('tri_selection(liste_cr)', number = 10, globals = globals()))

print("Patienter ...")
plt.plot(abscisse, ordonnee, "ro-", label = "Tri selection") # en rouge
plt.legend()
print("C'est bon !")
plt.show()
