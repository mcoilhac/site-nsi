def fonction_puissance(x, n):
    """
    renvoie x à la puissance n
    """
    resultat = x
    for i in range(n-1):
        resultat = resultat * x
    return resultat


assert fonction_puissance(2, 3) == 8, "le résultat de 2^3 est 8"
assert fonction_puissance(2, 0) == 1, "le résultat de 2^0 est 1"
