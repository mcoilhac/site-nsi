# initialisation 
mois = 0  # Numéro du mois avant de commencere
somme_totale = ...

# itération sur les 12 mois de l'année :
for somme in range(..., ..., ...):  # la variable somme a ici un nom explicite
    mois = ...  # Le numéro du mois doit être incrémenté à chaque tour de boucle
    # calculez la somme à ajouter
    somme_a_ajouter = ...
    print("Mois numéro", mois, " Bob épargne ", somme_a_ajouter)
    
    # on ajoute la somme dans l'accumulateur :
    somme_totale = ...
    
# affichage du résultat :
print("Après 12 mois, Bob dispose de ", somme_totale," €")
