# pour le triangle de gauche : 
BC = 3
AC = 5
AB = 4
print("-----------------------------------")

print("AB = ", AC, "AC = ", AC, "BC = ", BC)
if AB**2 == AC**2 + BC**2  :
    print("ABC est un triangle rectangle en C")
elif AC**2 == AB**2 + BC**2 :
    print("ABC est un triangle rectangle en B")
elif 𝐵𝐶**2 == 𝐴𝐵**2 + 𝐴𝐶**2 :
    print("ABC est un triangle rectangle en A")
else :
    print("le triangle n'est pas rectangle")

print("-----------------------------------")

# pour le triangle de droite : 
AC = 3
AB = 5
BC = 4
print("AB = ", AC, "AC = ", AC, "BC = ", BC)
if AB**2 == AC**2 + BC**2  :
    print("ABC est un triangle rectangle en C")
elif AC**2 == AB**2 + BC**2 :
    print("ABC est un triangle rectangle en B")
elif 𝐵𝐶**2 == 𝐴𝐵**2 + 𝐴𝐶**2 :
    print("ABC est un triangle rectangle en A")
else :
    print("le triangle n'est pas rectangle")
    
print("-----------------------------------")
    
# pour un triangle quelconque : 
AC = 2
AB = 5
BC = 4
print("AB = ", AC, "AC = ", AC, "BC = ", BC)
if AB**2 == AC**2 + BC**2  :
    print("ABC est un triangle rectangle en C")
elif AC**2 == AB**2 + BC**2 :
    print("ABC est un triangle rectangle en B")
elif 𝐵𝐶**2 == 𝐴𝐵**2 + 𝐴𝐶**2 :
    print("ABC est un triangle rectangle en A")
else :
    print("le triangle n'est pas rectangle")

