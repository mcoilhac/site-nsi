nb_de_notes = 4

somme = 0
    
for i in range(nb_de_notes):
    note = int(input("Entrez la note numéro " + str(i + 1) + ":"))
        
    # on ajoute cette note dans le total seulement si elle est paire :
    if note % 2 == 0 :
        somme = somme + note

# calcul de la moyenne :
moyenne = somme/nb_de_notes

print("La moyenne est : ", moyenne)
