# Initialisation du mot de passe secret :
secret = 123456

# On demande une première fois le mot de passe qui est de type int :
mot_de_passe = ...
# Tant que ce n'est pas le bon, on redemande le mot de passe :
while ...
   ...
print("Vous pouvez entrer !")

