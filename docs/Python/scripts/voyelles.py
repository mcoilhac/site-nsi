def nb_voyelles(mot):
    voyelles = 'aeiouyAEIOUY'
    nb = 0
    for lettre in mot:
        if lettre in voyelles:
            nb = nb + 1
    return nb

assert nb_voyelles("Bonjour") == 3
assert nb_voyelles("") == 0
assert nb_voyelles("Mr") == 0
assert nb_voyelles("") == 0
assert nb_voyelles("Ahaa !") == 3
