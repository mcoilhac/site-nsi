On peut répondre en une seule ligne : 

```python title=''
def maxi_3(n1, n2, n3):
    return maxi_2(maxi_2(n1, n2), n3)
```

